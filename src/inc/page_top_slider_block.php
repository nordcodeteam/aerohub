<?php
$fields = get_fields(get_queried_object_id());
if (isset($fields['page_top_slider_image']) && ($fields['page_top_slider_image'] != '')) :
?>

<div class="page-top-slider-wrap">
    <div class="top-slider-bg desktop" style="background-image: url('<?php echo $fields['page_top_slider_image']; ?>');"></div>
    <div class="top-slider-bg mobile" style="background-image: url('<?php echo $fields['page_top_slider_image_mobile']; ?>');"></div>
    <div class="gradient-block"></div>
    <div class="content-wrap">
        <div class="grid-row no-flex">        
            <?php if (isset($fields['page_top_slider_main_header']) && ($fields['page_top_slider_main_header'] != '')) : ?>
            <div class="main-header"><?php echo $fields['page_top_slider_main_header']; ?></div>
            <?php endif; ?>

            <?php if (isset($fields['page_top_slider_content']) && ($fields['page_top_slider_content'] != '')) : ?>
            <div class="slider-content"><?php echo $fields['page_top_slider_content']; ?></div>
            <?php endif; ?>
        </div>
    </div>
</div>

<div class="arch-top">
    <div class="scroll-to" data-block-id="index-main-content" data-slider-scroll="true">
        <div class="bg"></div>   
        <div class="text"><?php echo get_queried_object()->post_title; ?></div>   
    </div>
    <div class="arch"></div>
</div>
<?php endif; ?>