<?php 
$fields = get_fields(get_queried_object_id()); 
$news_block_bg = $fields['news_block_bg'];
$news_block_mobile_bg = $fields['news_block_mobile_background'];

$recent_posts = wp_get_recent_posts( array('numberposts' => 1, 'post_type' => 'post') );
if (!empty($recent_posts)) :
    $post_content = get_extended( $recent_posts[0]['post_content'] );
    $post_excerpt = $post_content['main'];
?>
    <section class="news-block hide">        
        <div class="desktop-bg block-bg" style="<?php echo ($news_block_bg != '') ? 'background-image: url(\''.$news_block_bg.'\')' : ''; ?>"></div>
        <div class="mobile-bg block-bg" 
            style="<?php echo ($news_block_mobile_bg != '') ? 'background-image: url(\''.$news_block_mobile_bg.'\')' : 'background-image: url(\''.$news_block_bg.'\')'; ?>">></div>

        <div class="scroll-to" data-block-id="news-block">
            <div class="bg"></div>   
            <div class="text"><?php echo $recent_posts[0]['post_title']; ?></div>   
        </div>
        
        <div class="gradient-mask"></div>
        <div class="arch-mask"></div>

        <div class="content-wrap">   
            <div class="grid-row">         
                <div class="news-block-content content-wrap" id="news-block">
                    <div class="news-date"><?php echo date('F d, Y', strtotime($recent_posts[0]['post_date'])); ?></div>
                    <div class="news-title grid-content-header"><h3><?php echo $recent_posts[0]['post_title']; ?></h3></div>
                    <div class="news-content"><?php echo strip_tags( $post_excerpt ); ?></div>
                    <div class="read-more"><a href="<?php echo get_permalink($recent_posts[0]['ID']); ?>" alt="<?php echo $recent_posts[0]->post_title; ?>">Read</a></div>
                </div>
            </div>
        </div>
    </section>

<?php endif; ?>