<div class="content-wrap main-slide-wrap">
    <div class="grid-row">
		<div class="main-slide-content">
			
			<?php
			$slider_fields = get_queried_object(); 
			$slider_fields = get_fields($slider_fields->ID);
			?>

			<?php if ($slider_fields['main_slider_header_text'] != '') : ?>
				<div class="header"><?php echo $slider_fields['main_slider_header_text']; ?></div>
			<?php endif; ?>

			<?php if ($slider_fields['main_slider_content_text'] != '') : ?>
				<div class="content"><?php echo $slider_fields['main_slider_content_text']; ?></div>
			<?php endif; ?>

			<?php if ($slider_fields['main_slider_link'] != '') : ?>
		        <div class="read-more"><a href="<?php echo $slider_fields['main_slider_link']; ?>" alt="More">More</a></div>
			<?php endif; ?>

		</div>
	</div>
</div>
