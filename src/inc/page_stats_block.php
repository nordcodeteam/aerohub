<?php

$fields = get_fields(get_queried_object_id()); 
 
?>

	
<?php if (isset($fields['statistics_block_header']) && ($fields['statistics_block_header'] != '')) : ?>
<div class="grid-row">
    <h2 class="grid-content-header top-margin long"><?php echo $fields['statistics_block_header']; ?></h2>
</div>
<?php endif; ?>

<?php if (isset($fields['statistics_block_graph_desktop']) && ($fields['statistics_block_graph_desktop'] != '')) : ?>
<div class="grid-row">
	<div class="stats-graph-holder grid-column-full">
	<?php if (isset($fields['statistics_block_graph_desktop']) && ($fields['statistics_block_graph_desktop'] != '')) : ?>
		<img class="desktop" src="<?php echo $fields['statistics_block_graph_desktop']; ?>">
	<?php endif; ?>	
	<?php if (isset($fields['statistics_block_graph_mobile']) && ($fields['statistics_block_graph_mobile'] != '')) : ?>
		<img class="mobile" src="<?php echo $fields['statistics_block_graph_mobile']; ?>">
    <?php endif; ?> 
    </div>
</div>
<?php endif; ?>	

<?php if (isset($fields['statistics_table']) && !empty($fields['statistics_table'])) : ?>
<?php if (isset($fields['statistics_table'][0]) && ($fields['statistics_table'][0]['column_one'] != '')) : ?>
<div class="grid-row">
    <table class="table grid-column-full">
        <tbody>        
            <?php $i = 0; ?>
            <?php foreach ($fields['statistics_table'] as $row) : ?>
                <tr class="row <?php echo ($i == 0) ? 'header' : ''; ?>">
                    <td class="cell"><?php echo $row['column_one']; ?></td>
                    <td class="cell"><?php echo $row['column_two']; ?></td>
                    <td class="cell"><?php echo $row['column_three']; ?></td>
                </tr>
                <?php $i++; ?>
            <?php endforeach; ?>
        </tbody>
    </table>
    
    <?php if (isset($fields['statistics_table_legend']) && !empty($fields['statistics_table_legend'])) : ?>
    <div class="table-legend">
        <?php foreach ($fields['statistics_table_legend'] as $row) : ?>
        <div class="table-legend-row"><?php echo $row['legend_row']; ?></div>
        <?php endforeach; ?>
    </div>
    <?php endif; ?>
</div>
<?php endif; ?>
<?php endif; ?>
