<?php
/*
WP Post Template: Location and connectivity
*/
?>


<?php get_header(); ?>

<?php include('inc/page_top_slider_block.php'); ?>

<?php
$post = get_queried_object();

$pattern = "/<h(.*?)<\/h/";
preg_match($pattern, wpautop($post->post_content), $matches);

$heading = '';
$content = '';
if (isset($matches[1]) && ($matches[1] != '')) {
    $heading = explode('>', $matches[1])[1];

    $get_content = explode($heading, strip_tags($post->post_content));

    if (!empty($get_content) && ($get_content[1] != '')) {
        $content = $get_content[1];
    }
}
?>

<div class="content-wrap">
    <div class="grid-row">
        <div class="content-wrap-text">
            <?php echo wpautop($post->post_content); ?>
        </div>
    </div>


</div>
    <div class="location-map europe-map desktop">
        <div class="map-bg">
            <?php include('img/location_map/inline-map.svg'); ?>
            <div class="circle-holder">
                <div class="circle circle-1"></div>
                <div class="circle circle-2"></div>
                <div class="circle circle-3"></div>
            </div>
            
            <div class="text-circle-wrapper" data-anim="base wrapper">
                <div class="text-circle" data-anim="base left"></div>
                <div class="text-circle" data-anim="base right"></div>
                <div class="text-block"><div class="text-holder">3 hrs <div class="small">flight range</div></div></div>
            </div>

            <div class="map-tab tooltip kaunas">
                <div class="map-popup">
                    <div class="text">Kaunas</div>
                    <div class="arrow"></div>
                </div>
            </div>
            <div class="map-tab tooltip aerohub">                
                <div class="map-popup">
                    <div class="logo"></div>
                    <div class="arrow"></div>
                </div>
            </div>
            <div class="map-tab box e67">E67</div>
            <div class="map-tab box e85">E85</div>
        </div>

        <div class="map-clouds"></div>

        <div class="location-tabs">
            <div class="location-tab kaunas" data-tab="kaunas">Kaunas lorem ipsum<div class="arrow"></div></div>
            <div class="location-tab e67" data-tab="e67">E67 Via Baltica<div class="arrow"></div></div>
            <div class="location-tab e85" data-tab="e85">E85 lorem ipsum<div class="arrow"></div></div>
            <div class="location-tab aerohub" data-tab="aerohub">AEROHUB KUN<div class="arrow"></div></div>
        </div>
    </div>

    <div class="location-map europe-map mobile">
        <div class="location-buttons">
            <div class="location-button kaunas" data-tab="kaunas">Kaunas lorem ipsum</div>
            <div class="location-button e67" data-tab="e67">E67 Via Baltica</div>
            <div class="location-button e85" data-tab="e85">E85 lorem ipsum</div>
            <div class="location-button aerohub" data-tab="aerohub">AEROHUB KUN</div>
        </div>
    </div>

    <div class="mobile-content-popup kaunas">
        <div class="header">
            <div class="title">Kaunas lorem ipsum</div>
            <div class="read-more"><a href="" alt="View all">Back</a></div>
        </div>

        <div class="content">
            <div class="pop-content">            
                <div class="image"></div>
                <div class="text">
                    <p>
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed 
                        diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam 
                        erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation 
                        ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
                    </p>
                </div>      
            </div>
        </div>
    </div>
<?php get_footer(); ?>