<?php get_header(); ?>

<?php include('inc/page_top_slider_block.php'); ?>

<div class="content-wrap">
    <?php include('inc/page_stats_block.php'); ?>
</div>

<div class="content-wrap">
    <?php include('inc/page_post_blocks.php'); ?>
</div>
<?php get_footer(); ?>