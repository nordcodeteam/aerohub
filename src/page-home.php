<?php /* Template Name: Home page */ ?>
<?php get_header(); ?>

<section class="content-block white">
	<div class="arch-top">
    	<div class="scroll-to" data-block-id="index-main-content">
            <div class="bg"></div>   
            <div class="text">Invest in Aerohub KUN</div>   
        </div>
        <div class="arch"></div>
	</div>

	<div class="content-wrap" id="index-main-content">
        <?php include('inc/page_post_blocks.php'); ?>
	</div>
</section>

<?php include('inc/page_news_block.php'); ?>

<?php get_footer(); ?>