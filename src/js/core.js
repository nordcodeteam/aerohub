// ==== CORE ==== //

;(function($){

    var scrollTop = 0;
    var menuSourceFixed = false;
	var burgerMenu = {
		init: function() {
			var button = $('.burger-menu-icon');
			var menu = $('#burger-wrap');
			button.on('click tap', this.toggleMenu);

		},

		close: function() {
			var button = $('.burger-menu-icon');
			var menu = $('#burger-wrap');

			button.removeClass('active');
			menu.removeClass('active');
            $('body').removeClass('disable-scroll');
            $('body').removeClass('fixed');
            $('html, body').animate({
                scrollTop: scrollTop
            }, 1);
        },  
        open: function() {
            var button = $('.burger-menu-icon');
            var menu = $('#burger-wrap');

            button.addClass('active');
            menu.addClass('active');
            scrollTop = $(window).scrollTop();

            menu.one('webkitTransitionEnd otransitionend msTransitionEnd transitionend', function() {
                $('body').addClass('disable-scroll');
                $('body').addClass('fixed');
            });
        },

        toggleMenu: function() {
            var button = $('.burger-menu-icon');
            var menu = $('#burger-wrap');

            button.toggleClass('active');
            button.addClass('clicked');
            menu.toggleClass('active');

            if ($(window).width() < 481) {            
                if ($('body').hasClass('fixed')) {
                    $('body').removeClass('fixed');
                    menuSourceFixed = false;
                    $('html, body').animate({
                        scrollTop: scrollTop
                    }, 0);
                } else {
                    if (fixedMenuActive) {
                        menuSourceFixed = true;
                    }
                    scrollTop = $(window).scrollTop();

                    $('body').addClass('fixed');
                }
            } else {
                if ($('body').hasClass('no-scroll')) {
                    $('body').removeClass('no-scroll');
                     menuSourceFixed = false;
                } else {
                    $('body').addClass('no-scroll');
                    if (fixedMenuActive) {
                        menuSourceFixed = true;
                    }
                }
            }
            fixedMenu();
		}
	};

    var scrollTo = {
        init: function() {
            $('.scroll-to').bind('click tap', function(){
                if ($(this).data('slider-scroll') == true) {
                    $('html, body').animate({
                        scrollTop: $('.page-top-slider-wrap').offset().top + $('.page-top-slider-wrap').outerHeight() + $('.arch-top').outerHeight() - $('.page-header.page').height()
                    }, 300);
                } else {              
                    $('html, body').animate({
                        scrollTop: $('#' + $(this).data('block-id')).offset().top - $('.page-header.page').height()
                    }, 300);
                }
            });
        }
    };

    var inview = {
        init: function() {
            var delayTime = 300;
            if ($('.post-blocks .post-block').length != 0) {     
                $('.post-blocks').each(function(){
                    $.each($(this).find('.post-block'), function(index, value){
                        new Waypoint.Inview({
                            element: $(value)[0],
                            enter: function(direction) {
                                $(value)
                                    .removeClass('hide-down')
                                    .removeClass('hide-up')
                                    .addClass('show');

                            },
                            entered: function(direction) {
                                $(value).removeClass('hide-up').removeClass('hide-down');

                                $(value).addClass('show');
                            },
                            exited: function(direction) {
                                $(value)
                                    .removeClass('show')
                                    .addClass('hide-' + direction);
                            }
                        })
                    });
                });     
            }

            if ($('.grid-content-header').length != 0) {
                $.each($('.grid-content-header'), function(index, value){
                        new Waypoint.Inview({
                            element: $(value)[0],
                            enter: function(direction) {
                                $(value)
                                    .removeClass('hide')
                                    .addClass('show');

                            },
                            entered: function(direction) {
                                $(value).removeClass('hide');
                                $(value).addClass('show');
                            },
                            exited: function(direction) {
                                $(value)
                                    .removeClass('show')
                                    .addClass('hide');
                            }
                        })
                });
            }
            if ($('.page-top-slider-wrap .main-header').length != 0) {
                $.each($('.page-top-slider-wrap .main-header'), function(index, value){
                        new Waypoint.Inview({
                            element: $(value)[0],
                            enter: function(direction) {
                                $(value)
                                    .removeClass('hide')
                                    .addClass('show');

                            },
                            entered: function(direction) {
                                $(value).removeClass('hide');
                                $(value).addClass('show');
                            },
                            exited: function(direction) {
                                $(value)
                                    .removeClass('show')
                                    .addClass('hide');
                            }
                        })
                });
            }
            if ($('.page-top-slider-wrap .slider-content').length != 0) {
                $.each($('.page-top-slider-wrap .slider-content'), function(index, value){
                        new Waypoint.Inview({
                            element: $(value)[0],
                            enter: function(direction) {
                                $(value)
                                    .removeClass('hide')
                                    .addClass('show');

                            },
                            entered: function(direction) {
                                $(value).removeClass('hide');
                                $(value).addClass('show');
                            },
                            exited: function(direction) {
                                $(value)
                                    .removeClass('show')
                                    .addClass('hide');
                            }
                        })
                });
            }

            if ($('.news-block').length != 0) {
                new Waypoint.Inview({
                    element: $('.news-block .news-content')[0],
                    entered: function(direction) {
                        $('.news-block')
                            .removeClass('hide')
                            .addClass('show');
                    }
                });
                new Waypoint.Inview({
                    element: $('.news-block')[0],
                    exited: function(direction) {
                        $('.news-block')
                            .removeClass('show')
                            .addClass('hide');
                    }
                });
            }
        } 
    };

    var mapAnimation = {
        position: function(){
            $('header .circle-holder').css({
                left: $('header .map-svg #kaunas').offset().left,
                top: $('header .map-svg #kaunas').offset().top 
            });
            $('header .map-popup-holder').css({
                left: $('header .map-svg #kaunas').offset().left - $('header .map-popup').width(),
                top: $('header .map-svg #kaunas').offset().top - $('header .map-popup').height() - $('header .map-popup .arrow').outerHeight() - 10
            });
            var circleWidth = ($('header .map-svg svg #start').offset().left - $('header .map-svg svg #destination').offset().left)
            circleWidth = (circleWidth * 2) + 17;
            $('header .circle-holder .circle-3').css({width: circleWidth, height: circleWidth});
            $('header .circle-holder .circle-2').css({width: circleWidth + 80, height: circleWidth + 80});
            $('header .circle-holder .circle-1').css({width: circleWidth + 200, height: circleWidth + 200});

            $('header .text-circle-wrapper').css({left: $('header .map-svg svg #destination').offset().left });
        },
        init: function(){
            var circleWidth = ($('header .map-svg svg #start').offset().left - $('header .map-svg svg #destination').offset().left)
            circleWidth = (circleWidth * 2) + 17;
            $('header .circle-holder .circle-3').css({width: circleWidth, height: circleWidth});
            $('header .circle-holder .circle-2').css({width: circleWidth + 80, height: circleWidth + 80});
            $('header .circle-holder .circle-1').css({width: circleWidth + 200, height: circleWidth + 200});
            $('header .text-circle-wrapper').css({left: $('header .map-svg svg #destination').offset().left});
            $('header .slider-overlay').fadeOut(500, function(){

                $('header .main-slide-wrap').addClass('animate').one('webkitTransitionEnd otransitionend msTransitionEnd transitionend', function() {
                    $('header .circle-holder').addClass('animate');
                    $('header .map-popup-holder').addClass('animate');
                });
                $('header .circle-holder .circle-1').one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function() {
                    $('header .text-circle-wrapper').addClass('animate');
                });
            });
        }
    };

    var scrollTop = 0;
    var kaunasMap = {
        position: function() {
            $('.kaunas-map .map-bg .map-tab.kaunas').css({
                left: $('.kaunas-map .map-bg #point-kaunas').offset().left - 80,
                top: $('.kaunas-map .map-bg #point-kaunas').offset().top - 34 - jQuery('.kaunas-map').offset().top
            });
            $('.kaunas-map .map-bg .map-tab.e67').css({
                left: $('.kaunas-map .map-bg #point-e67').offset().left  - 10,
                top: $('.kaunas-map .map-bg #point-e67').offset().top - jQuery('.kaunas-map').offset().top
            });
            $('.kaunas-map .map-bg .map-tab.e85').css({
                left: $('.kaunas-map .map-bg #point-e85').offset().left - 20,
                top: $('.kaunas-map .map-bg #point-e85').offset().top - jQuery('.kaunas-map').offset().top
            });
            $('.kaunas-map .map-bg .map-tab.aerohub').css({
                left: $('.kaunas-map .map-bg #point-aerohub').offset().left,
                top: $('.kaunas-map .map-bg #point-aerohub').offset().top + 10 - jQuery('.kaunas-map').offset().top
            });

            var maxHeight = 0;
            $('.mobile-content-popup').each(function(){
                if ($(this).outerHeight() > maxHeight) {
                    maxHeight = $(this).outerHeight();
                }
            });
            $('.mobile-content-popup-wrap').css('height', maxHeight);
        },
        init: function() {
            $('.kaunas-map .map-bg .map-tab.kaunas').addClass('active');
            $('.kaunas-map .location-tab.kaunas').addClass('active');
            $('.kaunas-map svg #kaunas_stroke path').attr('stroke', '#00c17a');
            $('.kaunas-map svg #kaunas_fill path').attr('fill', '#e0f1eb');

            $('.kaunas-map .location-tab').bind('click tap', function(){
                if ($(this).data('tab') == "kaunas") {
                    $('.kaunas-map svg #kaunas_stroke path').attr('stroke', '#00c17a');
                    $('.kaunas-map svg #kaunas_fill path').attr('fill', '#e0f1eb');
                } else {
                    $('.kaunas-map svg #kaunas_stroke path').attr('stroke', '#6787D2');
                    $('.kaunas-map svg #kaunas_fill path').attr('fill', '#E8E8E8');
                }

                if ($(this).data('tab') == "aerohub") {
                    $('.kaunas-map svg #aerohub path').attr('fill', '#00c17a');
                } else {
                    $('.kaunas-map svg #aerohub path').attr('fill', '#94A6CF');
                }
                if ($(this).data('tab') == "e67") {
                    $('.kaunas-map svg #E67 path').attr('stroke', '#00c17a');
                } else {
                    $('.kaunas-map svg #E67 path').attr('stroke', '#859DD9');
                }
                if ($(this).data('tab') == "e85") {
                    $('.kaunas-map svg #E85 path').attr('stroke', '#00c17a');
                } else {
                    $('.kaunas-map svg #E85 path').attr('stroke', '#859DD9');
                }

                $('.kaunas-map .location-tab').removeClass('active');
                $(this).addClass('active');
                $('.kaunas-map .map-bg .map-tab').removeClass('active');
                $('.kaunas-map .map-bg .map-tab.' + $(this).data('tab')).addClass('active');

            });

            $('.location-buttons .location-button').bind('click tap', function(){
                $('.location-buttons .location-button').removeClass('active');
                $(this).addClass('active');

                $('.mobile-content-popup').removeClass('active');
                $('.mobile-content-popup.' + $(this).data('tab')).addClass('active');
            });

        }
    };


    var europeMap = {
        position: function(){
            $('.europe-map .circle-holder').css({
                left: $('.location-map #kaunas-sostine').offset().left,
                top: $('.location-map #kaunas-sostine').offset().top - $('.location-map').offset().top
            });

            var maxHeight = 0;
            $('.mobile-content-popup').each(function(){
                if ($(this).outerHeight() > maxHeight) {
                    maxHeight = $(this).outerHeight();
                }
            });
            $('.mobile-content-popup-wrap').css('height', maxHeight);

            var circleWidth = ($('.europe-map .map-bg svg #kaunas-sostine').offset().left - $('.europe-map .map-bg svg #destination').offset().left)
            circleWidth = (circleWidth * 2) + 110;
            $('.europe-map .circle-holder .circle-3').css({width: circleWidth, height: circleWidth});
            $('.europe-map .circle-holder .circle-2').css({width: circleWidth + 80, height: circleWidth + 80});
            $('.europe-map .circle-holder .circle-1').css({width: circleWidth + 200, height: circleWidth + 200});
            $('.europe-map .text-circle-wrapper').css({left: $('.europe-map .map-bg svg #destination').offset().left - ($('.europe-map .text-circle-wrapper').outerWidth()/4)});
        },
        init: function(){
            var circleWidth = ($('.europe-map .map-bg svg #kaunas-sostine').offset().left - $('.europe-map .map-bg svg #destination').offset().left)
            circleWidth = (circleWidth * 2) + 110;
            $('.europe-map .circle-holder .circle-3').css({width: circleWidth, height: circleWidth});
            $('.europe-map .circle-holder .circle-2').css({width: circleWidth + 80, height: circleWidth + 80});
            $('.europe-map .circle-holder .circle-1').css({width: circleWidth + 200, height: circleWidth + 200});
            $('.europe-map .text-circle-wrapper').css({left: $('.europe-map .map-bg svg #destination').offset().left - ($('.europe-map .text-circle-wrapper').outerWidth()/4)});
            
            new Waypoint.Inview({
                element: $('.europe-map .location-tab:nth-child(1)')[0],
                entered: function(direction) {
                    $('.europe-map .circle-holder').addClass('animate');
                    $('.europe-map .circle-holder .circle-1').one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function() {
                        $('.europe-map .text-circle-wrapper').addClass('animate');
                    });
                    $('.europe-map .location-tab:nth-child(1)').addClass('active');

                    $('.europe-map svg #international_roads').show().attr('class', '');
                    $('.europe-map svg #rail_roads').show().attr('class', 'hide');
                }
            });

            
            $('.europe-map .location-tab').bind('click tap', function(){
   
                $('.europe-map .location-tab').removeClass('active');
                $(this).addClass('active');

                if ($(this).data('tab') == "international_roads") {
                    $('.europe-map svg #international_roads').attr('class', '');
                    $('.europe-map svg #rail_roads').attr('class', 'hide');
                } else {
                    $('.europe-map svg #international_roads').attr('class', 'hide');
                    $('.europe-map svg #rail_roads').attr('class', '');
                }
                
            });

            $('.location-buttons .location-button').bind('click tap', function(){
                $('.location-buttons .location-button').removeClass('active');
                $(this).addClass('active');

                $('.mobile-content-popup').removeClass('active');
                $('.mobile-content-popup.' + $(this).data('tab')).addClass('active');
            });
        }
    };
    var aboutSlides = {
        init: function() {
            var navItem = $('.about-slides-wrap .navigation .item');
            navItem.bind('click tap', function(){

                var slide = $(this).data('slide');
                $('.about-slide').removeClass('active');
                $('.about-slide.' + slide).addClass('active');
                

                // $('.about-slide.active').removeClass('active');
                // $('.about-slide.' + $(this).data('slide')).show().addClass('active').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend',   
                // function(e) {
                //     $('.about-slide:not(.active)').hide();
                // });

                navItem.removeClass('active');
                $(this).addClass('active');

                $('.compass').attr('class', 'compass');
                $('.compass').addClass($(this).data('slide'));
            });
            $('.location-buttons .location-button').bind('click tap', function(){
                $('.location-buttons .location-button').removeClass('active');
                $(this).addClass('active');

                $('.mobile-content-popup').removeClass('active');
                $('.mobile-content-popup.' + $(this).data('tab')).addClass('active');
            });
        },
        position: function() {
            $('.about-slides-wrap .about-slide').each(function(){
                var svg = $(this).find('svg');
                $(this).find('.tooltip').each(function(){
                    if (svg.find('.point-' + $(this).data('point')).length != 0) {
                        var offsetTop = 0;
                        var offsetLeft = 0;

                        if ($(this).hasClass('bottom-right')) {
                            offsetTop = $(this).outerHeight();
                            offsetLeft = $(this).outerWidth();
                        }
                        if ($(this).hasClass('bottom-left')) {
                            offsetTop = $(this).outerHeight();
                            offsetLeft = 0;
                        }
                        if ($(this).hasClass('top-left')) {
                            offsetTop = 0;
                            offsetLeft = 0;
                        }
                        if ($(this).hasClass('top-right')) {
                            offsetTop = 0;
                            offsetLeft = $(this).outerWidth();
                        }

                        $(this).css({
                            top: svg.find('.point-' + $(this).data('point')).offset().top - $('.about-slides-wrap .about-slide').offset().top - offsetTop,
                            left: svg.find('.point-' + $(this).data('point')).offset().left - offsetLeft,
                        });
                    }
                });
            });

            var maxHeight = 0;
            $('.mobile-content-popup').each(function(){
                if ($(this).outerHeight() > maxHeight) {
                    maxHeight = $(this).outerHeight();
                }
            });
            $('.mobile-content-popup-wrap').css('height', maxHeight);
        }
    };

    function resizeFullscreen() {
        if ($('header.home-page').length != 0) {
            $('header.home-page').css('height', $(window).height() - 50);
        }
    }

    var fixedMenuActive = false;
    function fixedMenu() {
        if ($('body').hasClass('home')) {
            if ($(window).scrollTop() > 0) {    
                $('.page-header.fixed-menu').addClass('show');
                $('.burger-menu-icon.real').css({
                    left: $('.page-header.home-page .burger-menu-icon.position').offset().left,
                    top: 20
                }); 
            } else {
                $('.page-header.fixed-menu').removeClass('show');     
                $('.burger-menu-icon.real').css({
                    left: $('.page-header.home-page .burger-menu-icon.position').offset().left,
                    top: 48
                }); 
            }
        } else {
            $('.burger-menu-icon.real').css({
                left: $('.burger-menu-icon.position').offset().left,
                top: 20
            });
        }
    }

	$(function(){
        burgerMenu.init();
        scrollTo.init();

        if ($('header .map-svg').length != 0) {
            mapAnimation.position();
        }
        if ($('.kaunas-map').length != 0) {
            kaunasMap.position();
            kaunasMap.init();
        }
        if ($('.europe-map').length != 0) {
            europeMap.position();
        }

        if ($('.about-slides-wrap').length != 0) {
            aboutSlides.init();
        }
        resizeFullscreen();
        fixedMenu();

    });

    $(window).load(function(){
        if ($('.sub-menu-item').length != 0) {
            $('body').addClass('large-padding');
        }
        inview.init();

        if ($('header .map-svg').length != 0) {
            mapAnimation.init();
        }
        if ($('.europe-map').length != 0) {
            europeMap.init();
        }
        if ($('.about-slides-wrap').length != 0) {
            aboutSlides.position();
        }

        $('.burger-menu-icon.real').addClass('show')
        $('.burger-menu-icon.position').addClass('show')
    });

    $(window).scroll(function(){
        fixedMenu();
    });

    $(window).resize(function(){
        resizeFullscreen();
        fixedMenu();
        if ($('header .map-svg').length != 0) {
            mapAnimation.position();
        }
        if ($('.kaunas-map').length != 0) {
            kaunasMap.position();
        }
        if ($('.europe-map').length != 0) {
            europeMap.position();
        }
        if ($('.about-slides-wrap').length != 0) {
            aboutSlides.position();
        }
    });
}(jQuery));

