<?php /* Template Name: About page */ ?>
<?php get_header(); ?>
<?php include('inc/page_top_slider_block.php'); ?>

<?php
$post = get_queried_object();

$pattern = "/<h(.*?)<\/h/";
preg_match($pattern, wpautop($post->post_content), $matches);

$heading = '';
$content = '';
if (isset($matches[1]) && ($matches[1] != '')) {
	$heading = explode('>', $matches[1])[1];

	$get_content = explode($heading, strip_tags($post->post_content));

	if (!empty($get_content) && ($get_content[1] != '')) {
		$content = $get_content[1];
	}
}
?>

<div class="content-wrap">
    <div class="grid-row">
        <div class="content-wrap-text">
            <?php echo wpautop($post->post_content); ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>