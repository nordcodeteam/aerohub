<?php get_header(); ?>


<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<script src="https://apis.google.com/js/platform.js" async defer></script>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

<?php 
$post = get_queried_object(); 
$recent_posts = get_posts( array(
    'numberposts' => 4, 
    'post_type' => 'post', 
    'orderby' => 'post_date',
    'order' => 'DESC',
    'post__not_in' => array( $post->ID )
) ); 


$news = get_page(array(
    'post_type' => 'page',
    'meta_key' => '_wp_page_template',
    'meta_value' => 'page-news.php'
));

if (!empty($pages)) {
    $news = $news[0];
}
?>

<div class="content-wrap">

    <div class="grid-row">
        <h2 class="grid-content-header top-margin green-border">Latest news</h2>
    </div>

    <div class="grid-row">
        <div class="single-post-content">        
            <div class="main-post-content">
                <div class="post-title"><?php echo $post->post_title; ?></div>
                <div class="post-date"><?php echo date('Y m d', strtotime($post->post_date)); ?></div>
                <div class="post-content"><?php echo wpautop($post->post_content); ?></div>

                <div class="share-box">
                    <div class="share-item facebook">
                        <div class="fb-like" data-href="<?php echo get_permalink($post->ID); ?>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
                    </div>
                    <div class="share-item google">        
                        <g:plusone size="medium"></g:plusone>
                    </div>
                    <div class="share-item twitter">
                        <a href="<?php echo get_permalink($post->ID); ?>" class="twitter-share-button"></a>
                    </div>
                </div>  
            </div>
            <div class="recent-posts">
                <div class="header">Recent news</div>

                <div class="recent-post-items">
                <?php if (!empty($recent_posts)) : ?>
                    <?php foreach ($recent_posts as $post) : ?>

                    <div class="post-item">
                        <?php $post_content = get_extended( $post->post_content ); ?>
                        <div class="post-item-image" style="background-image: url('<?php echo catch_that_image($post); ?>');"></div>
                        <div class="post-item-content-wrap">       
                            <div class="post-item-title"><?php echo $post->post_title; ?></div>
                            <div class="post-item-date"><?php echo date('Y m d', strtotime($post->post_date)); ?></div>
                            <div class="post-item-content"><?php echo strip_tags($post_content['main']); ?></div>
                        </div>
                        <a href="<?php echo get_permalink($post->ID); ?>"></a>
                    </div>
                    
                    <?php endforeach; ?>
                    
                    <?php if (!empty($news)) :?>
                    <div class="read-more"><a href="<?php echo get_permalink($news->ID); ?>" alt="View all">View all</a></div>
                    <?php endif; ?>
                <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>