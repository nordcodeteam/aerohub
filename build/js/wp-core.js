// ==== CORE ==== //

;(function($){

    var scrollTop = 0;
    var menuSourceFixed = false;
	var burgerMenu = {
		init: function() {
			var button = $('.burger-menu-icon');
			var menu = $('#burger-wrap');
			button.on('click tap', this.toggleMenu);

		},

		close: function() {
			var button = $('.burger-menu-icon');
			var menu = $('#burger-wrap');

			button.removeClass('active');
			menu.removeClass('active');
            $('body').removeClass('disable-scroll');
            $('body').removeClass('fixed');
            $('html, body').animate({
                scrollTop: scrollTop
            }, 1);
        },  
        open: function() {
            var button = $('.burger-menu-icon');
            var menu = $('#burger-wrap');

            button.addClass('active');
            menu.addClass('active');
            scrollTop = $(window).scrollTop();

            menu.one('webkitTransitionEnd otransitionend msTransitionEnd transitionend', function() {
                $('body').addClass('disable-scroll');
                $('body').addClass('fixed');
            });
        },

        toggleMenu: function() {
            var button = $('.burger-menu-icon');
            var menu = $('#burger-wrap');

            button.toggleClass('active');
            button.addClass('clicked');
            menu.toggleClass('active');

            if ($(window).width() < 481) {            
                if ($('body').hasClass('fixed')) {
                    $('body').removeClass('fixed');
                    menuSourceFixed = false;
                    $('html, body').animate({
                        scrollTop: scrollTop
                    }, 0);
                } else {
                    if (fixedMenuActive) {
                        menuSourceFixed = true;
                    }
                    scrollTop = $(window).scrollTop();

                    $('body').addClass('fixed');
                }
            } else {
                if ($('body').hasClass('no-scroll')) {
                    $('body').removeClass('no-scroll');
                     menuSourceFixed = false;
                } else {
                    $('body').addClass('no-scroll');
                    if (fixedMenuActive) {
                        menuSourceFixed = true;
                    }
                }
            }
            fixedMenu();
		}
	};

    var scrollTo = {
        init: function() {
            $('.scroll-to').bind('click tap', function(){
                if ($(this).data('slider-scroll') == true) {
                    $('html, body').animate({
                        scrollTop: $('.page-top-slider-wrap').offset().top + $('.page-top-slider-wrap').outerHeight() + $('.arch-top').outerHeight() - $('.page-header.page').height()
                    }, 300);
                } else {              
                    $('html, body').animate({
                        scrollTop: $('#' + $(this).data('block-id')).offset().top - $('.page-header.page').height()
                    }, 300);
                }
            });
        }
    };

    var inview = {
        init: function() {
            var delayTime = 300;
            if ($('.post-blocks .post-block').length != 0) {     
                $('.post-blocks').each(function(){
                    $.each($(this).find('.post-block'), function(index, value){
                        new Waypoint.Inview({
                            element: $(value)[0],
                            enter: function(direction) {
                                $(value)
                                    .removeClass('hide-down')
                                    .removeClass('hide-up')
                                    .addClass('show');

                            },
                            entered: function(direction) {
                                $(value).removeClass('hide-up').removeClass('hide-down');

                                $(value).addClass('show');
                            },
                            exited: function(direction) {
                                $(value)
                                    .removeClass('show')
                                    .addClass('hide-' + direction);
                            }
                        })
                    });
                });     
            }

            if ($('.grid-content-header').length != 0) {
                $.each($('.grid-content-header'), function(index, value){
                        new Waypoint.Inview({
                            element: $(value)[0],
                            enter: function(direction) {
                                $(value)
                                    .removeClass('hide')
                                    .addClass('show');

                            },
                            entered: function(direction) {
                                $(value).removeClass('hide');
                                $(value).addClass('show');
                            },
                            exited: function(direction) {
                                $(value)
                                    .removeClass('show')
                                    .addClass('hide');
                            }
                        })
                });
            }
            if ($('.page-top-slider-wrap .main-header').length != 0) {
                $.each($('.page-top-slider-wrap .main-header'), function(index, value){
                        new Waypoint.Inview({
                            element: $(value)[0],
                            enter: function(direction) {
                                $(value)
                                    .removeClass('hide')
                                    .addClass('show');

                            },
                            entered: function(direction) {
                                $(value).removeClass('hide');
                                $(value).addClass('show');
                            },
                            exited: function(direction) {
                                $(value)
                                    .removeClass('show')
                                    .addClass('hide');
                            }
                        })
                });
            }
            if ($('.page-top-slider-wrap .slider-content').length != 0) {
                $.each($('.page-top-slider-wrap .slider-content'), function(index, value){
                        new Waypoint.Inview({
                            element: $(value)[0],
                            enter: function(direction) {
                                $(value)
                                    .removeClass('hide')
                                    .addClass('show');

                            },
                            entered: function(direction) {
                                $(value).removeClass('hide');
                                $(value).addClass('show');
                            },
                            exited: function(direction) {
                                $(value)
                                    .removeClass('show')
                                    .addClass('hide');
                            }
                        })
                });
            }

            if ($('.news-block').length != 0) {
                new Waypoint.Inview({
                    element: $('.news-block .news-content')[0],
                    entered: function(direction) {
                        $('.news-block')
                            .removeClass('hide')
                            .addClass('show');
                    }
                });
                new Waypoint.Inview({
                    element: $('.news-block')[0],
                    exited: function(direction) {
                        $('.news-block')
                            .removeClass('show')
                            .addClass('hide');
                    }
                });
            }
        } 
    };

    var mapAnimation = {
        position: function(){
            $('header .circle-holder').css({
                left: $('header .map-svg #kaunas').offset().left,
                top: $('header .map-svg #kaunas').offset().top 
            });
            $('header .map-popup-holder').css({
                left: $('header .map-svg #kaunas').offset().left - $('header .map-popup').width(),
                top: $('header .map-svg #kaunas').offset().top - $('header .map-popup').height() - $('header .map-popup .arrow').outerHeight() - 10
            });
            var circleWidth = ($('header .map-svg svg #start').offset().left - $('header .map-svg svg #destination').offset().left)
            circleWidth = (circleWidth * 2) + 17;
            $('header .circle-holder .circle-3').css({width: circleWidth, height: circleWidth});
            $('header .circle-holder .circle-2').css({width: circleWidth + 80, height: circleWidth + 80});
            $('header .circle-holder .circle-1').css({width: circleWidth + 200, height: circleWidth + 200});

            $('header .text-circle-wrapper').css({left: $('header .map-svg svg #destination').offset().left + ($('header .text-circle-wrapper').outerWidth()*2) - ($('header .text-circle-wrapper').outerWidth() / 2)});
        },
        init: function(){
            var circleWidth = ($('header .map-svg svg #start').offset().left - $('header .map-svg svg #destination').offset().left)
            circleWidth = (circleWidth * 2) + 17;
            $('header .circle-holder .circle-3').css({width: circleWidth, height: circleWidth});
            $('header .circle-holder .circle-2').css({width: circleWidth + 80, height: circleWidth + 80});
            $('header .circle-holder .circle-1').css({width: circleWidth + 200, height: circleWidth + 200});
            $('header .text-circle-wrapper').css({left: $('header .map-svg svg #destination').offset().left + ($('header .text-circle-wrapper').outerWidth()*2) - ($('header .text-circle-wrapper').outerWidth() / 2)});
            $('header .slider-overlay').fadeOut(500, function(){

                $('header .main-slide-wrap').addClass('animate').one('webkitTransitionEnd otransitionend msTransitionEnd transitionend', function() {
                    $('header .circle-holder').addClass('animate');
                    $('header .map-popup-holder').addClass('animate');
                });
                $('header .circle-holder .circle-1').one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function() {
                    $('header .text-circle-wrapper').addClass('animate');
                });
            });
        }
    };

    var scrollTop = 0;
    var kaunasMap = {
        position: function() {
            $('.kaunas-map .map-bg .map-tab.kaunas').css({
                left: $('.kaunas-map .map-bg #point-kaunas').offset().left - 80,
                top: $('.kaunas-map .map-bg #point-kaunas').offset().top - 34 - jQuery('.kaunas-map').offset().top
            });
            $('.kaunas-map .map-bg .map-tab.e67').css({
                left: $('.kaunas-map .map-bg #point-e67').offset().left  - 10,
                top: $('.kaunas-map .map-bg #point-e67').offset().top - jQuery('.kaunas-map').offset().top
            });
            $('.kaunas-map .map-bg .map-tab.e85').css({
                left: $('.kaunas-map .map-bg #point-e85').offset().left - 20,
                top: $('.kaunas-map .map-bg #point-e85').offset().top - jQuery('.kaunas-map').offset().top
            });
            $('.kaunas-map .map-bg .map-tab.aerohub').css({
                left: $('.kaunas-map .map-bg #point-aerohub').offset().left,
                top: $('.kaunas-map .map-bg #point-aerohub').offset().top + 10 - jQuery('.kaunas-map').offset().top
            });

            var maxHeight = 0;
            $('.mobile-content-popup').each(function(){
                if ($(this).outerHeight() > maxHeight) {
                    maxHeight = $(this).outerHeight();
                }
            });
            $('.mobile-content-popup-wrap').css('height', maxHeight);
        },
        init: function() {
            $('.kaunas-map .map-bg .map-tab.kaunas').addClass('active');
            $('.kaunas-map .location-tab.kaunas').addClass('active');
            $('.kaunas-map svg #kaunas_stroke path').attr('stroke', '#00c17a');
            $('.kaunas-map svg #kaunas_fill path').attr('fill', '#e0f1eb');

            $('.kaunas-map .location-tab').bind('click tap', function(){
                if ($(this).data('tab') == "kaunas") {
                    $('.kaunas-map svg #kaunas_stroke path').attr('stroke', '#00c17a');
                    $('.kaunas-map svg #kaunas_fill path').attr('fill', '#e0f1eb');
                } else {
                    $('.kaunas-map svg #kaunas_stroke path').attr('stroke', '#6787D2');
                    $('.kaunas-map svg #kaunas_fill path').attr('fill', '#E8E8E8');
                }

                if ($(this).data('tab') == "aerohub") {
                    $('.kaunas-map svg #aerohub path').attr('fill', '#00c17a');
                } else {
                    $('.kaunas-map svg #aerohub path').attr('fill', '#94A6CF');
                }
                if ($(this).data('tab') == "e67") {
                    $('.kaunas-map svg #E67 path').attr('stroke', '#00c17a');
                } else {
                    $('.kaunas-map svg #E67 path').attr('stroke', '#859DD9');
                }
                if ($(this).data('tab') == "e85") {
                    $('.kaunas-map svg #E85 path').attr('stroke', '#00c17a');
                } else {
                    $('.kaunas-map svg #E85 path').attr('stroke', '#859DD9');
                }

                $('.kaunas-map .location-tab').removeClass('active');
                $(this).addClass('active');
                $('.kaunas-map .map-bg .map-tab').removeClass('active');
                $('.kaunas-map .map-bg .map-tab.' + $(this).data('tab')).addClass('active');

            });

            $('.location-buttons .location-button').bind('click tap', function(){
                $('.location-buttons .location-button').removeClass('active');
                $(this).addClass('active');

                $('.mobile-content-popup').removeClass('active');
                $('.mobile-content-popup.' + $(this).data('tab')).addClass('active');
            });

        }
    };


    var europeMap = {
        position: function(){
            $('.europe-map .circle-holder').css({
                left: $('.location-map #kaunas-sostine').offset().left,
                top: $('.location-map #kaunas-sostine').offset().top - $('.location-map').offset().top
            });
            $('.europe-map .map-tab.oslas').css({
                left: $('.europe-map #oslas-sostine').offset().left - 77,
                top: $('.europe-map #oslas-sostine').offset().top - jQuery('.europe-map').offset().top - 43
            });
            $('.europe-map .map-tab.aerohub').css({
                left: $('.europe-map #kaunas-sostine').offset().left + 5,
                top: $('.europe-map #kaunas-sostine').offset().top - jQuery('.europe-map').offset().top - 45
            });
            $('.europe-map .map-tab.tallinn').css({
                left: $('.europe-map #tallinn-sostine').offset().left + 8,
                top: $('.europe-map #tallinn-sostine').offset().top - jQuery('.europe-map').offset().top + 15
            });
            $('.europe-map .map-tab.warsaw').css({
                left: $('.europe-map #warsaw-sostine').offset().left - 78,
                top: $('.europe-map #warsaw-sostine').offset().top - jQuery('.europe-map').offset().top - 45
            });
            $('.europe-map .map-tab.moscow').css({
                left: $('.europe-map #moscow-sostine').offset().left - 75,
                top: $('.europe-map #moscow-sostine').offset().top - jQuery('.europe-map').offset().top - 52
            });
            $('.europe-map .map-tab.amsterdam').css({
                left: $('.europe-map #amsterdam-sostine').offset().left + 4,
                top: $('.europe-map #amsterdam-sostine').offset().top - jQuery('.europe-map').offset().top - 45
            });

            var maxHeight = 0;
            $('.mobile-content-popup').each(function(){
                if ($(this).outerHeight() > maxHeight) {
                    maxHeight = $(this).outerHeight();
                }
            });
            $('.mobile-content-popup-wrap').css('height', maxHeight);

            var circleWidth = ($('.europe-map .map-bg svg #kaunas-sostine').offset().left - $('.europe-map .map-bg svg #destination').offset().left)
            circleWidth = (circleWidth * 2) + 20;
            $('.europe-map .circle-holder .circle-3').css({width: circleWidth, height: circleWidth});
            $('.europe-map .circle-holder .circle-2').css({width: circleWidth + 80, height: circleWidth + 80});
            $('.europe-map .circle-holder .circle-1').css({width: circleWidth + 200, height: circleWidth + 200});
            $('.europe-map .text-circle-wrapper').css({left: $('.europe-map .map-bg svg #destination').offset().left - ($('.europe-map .text-circle-wrapper').outerWidth()/4)});
        },
        init: function(){
            var circleWidth = ($('.europe-map .map-bg svg #kaunas-sostine').offset().left - $('.europe-map .map-bg svg #destination').offset().left)
            circleWidth = (circleWidth * 2) + 20;
            $('.europe-map .circle-holder .circle-3').css({width: circleWidth, height: circleWidth});
            $('.europe-map .circle-holder .circle-2').css({width: circleWidth + 80, height: circleWidth + 80});
            $('.europe-map .circle-holder .circle-1').css({width: circleWidth + 200, height: circleWidth + 200});
            $('.europe-map .text-circle-wrapper').css({left: $('.europe-map .map-bg svg #destination').offset().left - ($('.europe-map .text-circle-wrapper').outerWidth()/4)});
            
            new Waypoint.Inview({
                element: $('.europe-map .location-tab:nth-child(1)')[0],
                entered: function(direction) {
                    $('.europe-map .circle-holder').addClass('animate');
                    $('.europe-map .circle-holder .circle-1').one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function() {
                        $('.europe-map .text-circle-wrapper').addClass('animate');
                    });
                    $('.europe-map .location-tab:nth-child(1)').addClass('active');
                    $('.europe-map svg #' + $('.europe-map .location-tab.active').data('tab')).attr('class', 'target');
                    
                    if ($('.europe-map .location-tab.active').data('tab') == "via_baltica") {
                        $('.europe-map .map-tab.warsaw').addClass('active');
                        $('.europe-map .map-tab.tallinn').addClass('active');
                        $('.europe-map .map-tab.aerohub').addClass('active');
                    }
                }
            });

            
            $('.europe-map .location-tab').bind('click tap', function(){
                $('.europe-map svg .target').each(function(){
                    $(this).attr('class', 'hide target');
                });
                $('.europe-map .location-tab').removeClass('active');
                $(this).addClass('active');

                $('.europe-map svg #' + $(this).data('tab')).attr('class', 'target');

                $('.europe-map .map-tab').removeClass('active');
                if ($(this).data('tab') == "via_baltica") {
                    $('.europe-map .map-tab.warsaw').addClass('active');
                    $('.europe-map .map-tab.tallinn').addClass('active');
                    $('.europe-map .map-tab.aerohub').addClass('active');
                }
                if ($(this).data('tab') == "europien_gauge") {
                    $('.europe-map .map-tab.warsaw').addClass('active');
                }
                if ($(this).data('tab') == "russian_gauge") {
                    $('.europe-map .map-tab.tallinn').addClass('active');
                    $('.europe-map .map-tab.moscow').addClass('active');
                }


            });

            $('.location-buttons .location-button').bind('click tap', function(){
                $('.location-buttons .location-button').removeClass('active');
                $(this).addClass('active');

                $('.mobile-content-popup').removeClass('active');
                $('.mobile-content-popup.' + $(this).data('tab')).addClass('active');
            });
        }
    };
    var aboutSlides = {
        init: function() {
            var navItem = $('.about-slides-wrap .navigation .item');
            navItem.bind('click tap', function(){

                var slide = $(this).data('slide');
                $('.about-slide').removeClass('active');
                $('.about-slide.' + slide).addClass('active');
                

                // $('.about-slide.active').removeClass('active');
                // $('.about-slide.' + $(this).data('slide')).show().addClass('active').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend',   
                // function(e) {
                //     $('.about-slide:not(.active)').hide();
                // });

                navItem.removeClass('active');
                $(this).addClass('active');

                $('.compass').attr('class', 'compass');
                $('.compass').addClass($(this).data('slide'));
            });
            $('.location-buttons .location-button').bind('click tap', function(){
                $('.location-buttons .location-button').removeClass('active');
                $(this).addClass('active');

                $('.mobile-content-popup').removeClass('active');
                $('.mobile-content-popup.' + $(this).data('tab')).addClass('active');
            });
        },
        position: function() {
            $('.about-slides-wrap .about-slide').each(function(){
                var svg = $(this).find('svg');
                $(this).find('.tooltip').each(function(){
                    if (svg.find('.point-' + $(this).data('point')).length != 0) {
                        var offsetTop = 0;
                        var offsetLeft = 0;

                        if ($(this).hasClass('bottom-right')) {
                            offsetTop = $(this).outerHeight();
                            offsetLeft = $(this).outerWidth();
                        }
                        if ($(this).hasClass('bottom-left')) {
                            offsetTop = $(this).outerHeight();
                            offsetLeft = 0;
                        }
                        if ($(this).hasClass('top-left')) {
                            offsetTop = 0;
                            offsetLeft = 0;
                        }
                        if ($(this).hasClass('top-right')) {
                            offsetTop = 0;
                            offsetLeft = $(this).outerWidth();
                        }

                        $(this).css({
                            top: svg.find('.point-' + $(this).data('point')).offset().top - $('.about-slides-wrap .about-slide').offset().top - offsetTop,
                            left: svg.find('.point-' + $(this).data('point')).offset().left - offsetLeft,
                        });
                    }
                });
            });

            var maxHeight = 0;
            $('.mobile-content-popup').each(function(){
                if ($(this).outerHeight() > maxHeight) {
                    maxHeight = $(this).outerHeight();
                }
            });
            $('.mobile-content-popup-wrap').css('height', maxHeight);
        }
    };

    function resizeFullscreen() {
        if ($('header.home-page').length != 0) {
            $('header.home-page').css('height', $(window).height() - 50);
        }
    }

    var fixedMenuActive = false;
    function fixedMenu() {
        if ($('body').hasClass('home')) {
            if ($(window).scrollTop() > 0) {    
                $('.page-header.fixed-menu').addClass('show');
                $('.burger-menu-icon.real').css({
                    left: $('.page-header.home-page .burger-menu-icon.position').offset().left,
                    top: 20
                }); 
            } else {
                $('.page-header.fixed-menu').removeClass('show');     
                $('.burger-menu-icon.real').css({
                    left: $('.page-header.home-page .burger-menu-icon.position').offset().left,
                    top: 48
                }); 
            }
        } else {
            $('.burger-menu-icon.real').css({
                left: $('.burger-menu-icon.position').offset().left,
                top: 20
            });
        }
    }

	$(function(){
        burgerMenu.init();
        scrollTo.init();

        if ($('header .map-svg').length != 0) {
            mapAnimation.position();
        }
        if ($('.kaunas-map').length != 0) {
            kaunasMap.position();
            kaunasMap.init();
        }
        if ($('.europe-map').length != 0) {
            europeMap.position();
        }

        if ($('.about-slides-wrap').length != 0) {
            aboutSlides.init();
        }
        resizeFullscreen();
        fixedMenu();

    });

    $(window).load(function(){
        if ($('.sub-menu-item').length != 0) {
            $('body').addClass('large-padding');
        }
        inview.init();

        if ($('header .map-svg').length != 0) {
            mapAnimation.init();
        }
        if ($('.europe-map').length != 0) {
            europeMap.init();
        }
        if ($('.about-slides-wrap').length != 0) {
            aboutSlides.position();
        }

        $('.burger-menu-icon.real').addClass('show')
        $('.burger-menu-icon.position').addClass('show')
    });

    $(window).scroll(function(){
        fixedMenu();
    });

    $(window).resize(function(){
        resizeFullscreen();
        fixedMenu();
        if ($('header .map-svg').length != 0) {
            mapAnimation.position();
        }
        if ($('.kaunas-map').length != 0) {
            kaunasMap.position();
        }
        if ($('.europe-map').length != 0) {
            europeMap.position();
        }
        if ($('.about-slides-wrap').length != 0) {
            aboutSlides.position();
        }
    });
}(jQuery));


/*!
Waypoints - 4.0.0
Copyright © 2011-2015 Caleb Troughton
Licensed under the MIT license.
https://github.com/imakewebthings/waypoints/blog/master/licenses.txt
*/
!function(){"use strict";function t(o){if(!o)throw new Error("No options passed to Waypoint constructor");if(!o.element)throw new Error("No element option passed to Waypoint constructor");if(!o.handler)throw new Error("No handler option passed to Waypoint constructor");this.key="waypoint-"+e,this.options=t.Adapter.extend({},t.defaults,o),this.element=this.options.element,this.adapter=new t.Adapter(this.element),this.callback=o.handler,this.axis=this.options.horizontal?"horizontal":"vertical",this.enabled=this.options.enabled,this.triggerPoint=null,this.group=t.Group.findOrCreate({name:this.options.group,axis:this.axis}),this.context=t.Context.findOrCreateByElement(this.options.context),t.offsetAliases[this.options.offset]&&(this.options.offset=t.offsetAliases[this.options.offset]),this.group.add(this),this.context.add(this),i[this.key]=this,e+=1}var e=0,i={};t.prototype.queueTrigger=function(t){this.group.queueTrigger(this,t)},t.prototype.trigger=function(t){this.enabled&&this.callback&&this.callback.apply(this,t)},t.prototype.destroy=function(){this.context.remove(this),this.group.remove(this),delete i[this.key]},t.prototype.disable=function(){return this.enabled=!1,this},t.prototype.enable=function(){return this.context.refresh(),this.enabled=!0,this},t.prototype.next=function(){return this.group.next(this)},t.prototype.previous=function(){return this.group.previous(this)},t.invokeAll=function(t){var e=[];for(var o in i)e.push(i[o]);for(var n=0,r=e.length;r>n;n++)e[n][t]()},t.destroyAll=function(){t.invokeAll("destroy")},t.disableAll=function(){t.invokeAll("disable")},t.enableAll=function(){t.invokeAll("enable")},t.refreshAll=function(){t.Context.refreshAll()},t.viewportHeight=function(){return window.innerHeight||document.documentElement.clientHeight},t.viewportWidth=function(){return document.documentElement.clientWidth},t.adapters=[],t.defaults={context:window,continuous:!0,enabled:!0,group:"default",horizontal:!1,offset:0},t.offsetAliases={"bottom-in-view":function(){return this.context.innerHeight()-this.adapter.outerHeight()},"right-in-view":function(){return this.context.innerWidth()-this.adapter.outerWidth()}},window.Waypoint=t}(),function(){"use strict";function t(t){window.setTimeout(t,1e3/60)}function e(t){this.element=t,this.Adapter=n.Adapter,this.adapter=new this.Adapter(t),this.key="waypoint-context-"+i,this.didScroll=!1,this.didResize=!1,this.oldScroll={x:this.adapter.scrollLeft(),y:this.adapter.scrollTop()},this.waypoints={vertical:{},horizontal:{}},t.waypointContextKey=this.key,o[t.waypointContextKey]=this,i+=1,this.createThrottledScrollHandler(),this.createThrottledResizeHandler()}var i=0,o={},n=window.Waypoint,r=window.onload;e.prototype.add=function(t){var e=t.options.horizontal?"horizontal":"vertical";this.waypoints[e][t.key]=t,this.refresh()},e.prototype.checkEmpty=function(){var t=this.Adapter.isEmptyObject(this.waypoints.horizontal),e=this.Adapter.isEmptyObject(this.waypoints.vertical);t&&e&&(this.adapter.off(".waypoints"),delete o[this.key])},e.prototype.createThrottledResizeHandler=function(){function t(){e.handleResize(),e.didResize=!1}var e=this;this.adapter.on("resize.waypoints",function(){e.didResize||(e.didResize=!0,n.requestAnimationFrame(t))})},e.prototype.createThrottledScrollHandler=function(){function t(){e.handleScroll(),e.didScroll=!1}var e=this;this.adapter.on("scroll.waypoints",function(){(!e.didScroll||n.isTouch)&&(e.didScroll=!0,n.requestAnimationFrame(t))})},e.prototype.handleResize=function(){n.Context.refreshAll()},e.prototype.handleScroll=function(){var t={},e={horizontal:{newScroll:this.adapter.scrollLeft(),oldScroll:this.oldScroll.x,forward:"right",backward:"left"},vertical:{newScroll:this.adapter.scrollTop(),oldScroll:this.oldScroll.y,forward:"down",backward:"up"}};for(var i in e){var o=e[i],n=o.newScroll>o.oldScroll,r=n?o.forward:o.backward;for(var s in this.waypoints[i]){var a=this.waypoints[i][s],l=o.oldScroll<a.triggerPoint,h=o.newScroll>=a.triggerPoint,p=l&&h,u=!l&&!h;(p||u)&&(a.queueTrigger(r),t[a.group.id]=a.group)}}for(var c in t)t[c].flushTriggers();this.oldScroll={x:e.horizontal.newScroll,y:e.vertical.newScroll}},e.prototype.innerHeight=function(){return this.element==this.element.window?n.viewportHeight():this.adapter.innerHeight()},e.prototype.remove=function(t){delete this.waypoints[t.axis][t.key],this.checkEmpty()},e.prototype.innerWidth=function(){return this.element==this.element.window?n.viewportWidth():this.adapter.innerWidth()},e.prototype.destroy=function(){var t=[];for(var e in this.waypoints)for(var i in this.waypoints[e])t.push(this.waypoints[e][i]);for(var o=0,n=t.length;n>o;o++)t[o].destroy()},e.prototype.refresh=function(){var t,e=this.element==this.element.window,i=e?void 0:this.adapter.offset(),o={};this.handleScroll(),t={horizontal:{contextOffset:e?0:i.left,contextScroll:e?0:this.oldScroll.x,contextDimension:this.innerWidth(),oldScroll:this.oldScroll.x,forward:"right",backward:"left",offsetProp:"left"},vertical:{contextOffset:e?0:i.top,contextScroll:e?0:this.oldScroll.y,contextDimension:this.innerHeight(),oldScroll:this.oldScroll.y,forward:"down",backward:"up",offsetProp:"top"}};for(var r in t){var s=t[r];for(var a in this.waypoints[r]){var l,h,p,u,c,d=this.waypoints[r][a],f=d.options.offset,w=d.triggerPoint,y=0,g=null==w;d.element!==d.element.window&&(y=d.adapter.offset()[s.offsetProp]),"function"==typeof f?f=f.apply(d):"string"==typeof f&&(f=parseFloat(f),d.options.offset.indexOf("%")>-1&&(f=Math.ceil(s.contextDimension*f/100))),l=s.contextScroll-s.contextOffset,d.triggerPoint=y+l-f,h=w<s.oldScroll,p=d.triggerPoint>=s.oldScroll,u=h&&p,c=!h&&!p,!g&&u?(d.queueTrigger(s.backward),o[d.group.id]=d.group):!g&&c?(d.queueTrigger(s.forward),o[d.group.id]=d.group):g&&s.oldScroll>=d.triggerPoint&&(d.queueTrigger(s.forward),o[d.group.id]=d.group)}}return n.requestAnimationFrame(function(){for(var t in o)o[t].flushTriggers()}),this},e.findOrCreateByElement=function(t){return e.findByElement(t)||new e(t)},e.refreshAll=function(){for(var t in o)o[t].refresh()},e.findByElement=function(t){return o[t.waypointContextKey]},window.onload=function(){r&&r(),e.refreshAll()},n.requestAnimationFrame=function(e){var i=window.requestAnimationFrame||window.mozRequestAnimationFrame||window.webkitRequestAnimationFrame||t;i.call(window,e)},n.Context=e}(),function(){"use strict";function t(t,e){return t.triggerPoint-e.triggerPoint}function e(t,e){return e.triggerPoint-t.triggerPoint}function i(t){this.name=t.name,this.axis=t.axis,this.id=this.name+"-"+this.axis,this.waypoints=[],this.clearTriggerQueues(),o[this.axis][this.name]=this}var o={vertical:{},horizontal:{}},n=window.Waypoint;i.prototype.add=function(t){this.waypoints.push(t)},i.prototype.clearTriggerQueues=function(){this.triggerQueues={up:[],down:[],left:[],right:[]}},i.prototype.flushTriggers=function(){for(var i in this.triggerQueues){var o=this.triggerQueues[i],n="up"===i||"left"===i;o.sort(n?e:t);for(var r=0,s=o.length;s>r;r+=1){var a=o[r];(a.options.continuous||r===o.length-1)&&a.trigger([i])}}this.clearTriggerQueues()},i.prototype.next=function(e){this.waypoints.sort(t);var i=n.Adapter.inArray(e,this.waypoints),o=i===this.waypoints.length-1;return o?null:this.waypoints[i+1]},i.prototype.previous=function(e){this.waypoints.sort(t);var i=n.Adapter.inArray(e,this.waypoints);return i?this.waypoints[i-1]:null},i.prototype.queueTrigger=function(t,e){this.triggerQueues[e].push(t)},i.prototype.remove=function(t){var e=n.Adapter.inArray(t,this.waypoints);e>-1&&this.waypoints.splice(e,1)},i.prototype.first=function(){return this.waypoints[0]},i.prototype.last=function(){return this.waypoints[this.waypoints.length-1]},i.findOrCreate=function(t){return o[t.axis][t.name]||new i(t)},n.Group=i}(),function(){"use strict";function t(t){this.$element=e(t)}var e=window.jQuery,i=window.Waypoint;e.each(["innerHeight","innerWidth","off","offset","on","outerHeight","outerWidth","scrollLeft","scrollTop"],function(e,i){t.prototype[i]=function(){var t=Array.prototype.slice.call(arguments);return this.$element[i].apply(this.$element,t)}}),e.each(["extend","inArray","isEmptyObject"],function(i,o){t[o]=e[o]}),i.adapters.push({name:"jquery",Adapter:t}),i.Adapter=t}(),function(){"use strict";function t(t){return function(){var i=[],o=arguments[0];return t.isFunction(arguments[0])&&(o=t.extend({},arguments[1]),o.handler=arguments[0]),this.each(function(){var n=t.extend({},o,{element:this});"string"==typeof n.context&&(n.context=t(this).closest(n.context)[0]),i.push(new e(n))}),i}}var e=window.Waypoint;window.jQuery&&(window.jQuery.fn.waypoint=t(window.jQuery)),window.Zepto&&(window.Zepto.fn.waypoint=t(window.Zepto))}();
/*!
Waypoints Inview Shortcut - 4.0.0
Copyright © 2011-2015 Caleb Troughton
Licensed under the MIT license.
https://github.com/imakewebthings/waypoints/blog/master/licenses.txt
*/
!function(){"use strict";function t(){}function e(t){this.options=i.Adapter.extend({},e.defaults,t),this.axis=this.options.horizontal?"horizontal":"vertical",this.waypoints=[],this.element=this.options.element,this.createWaypoints()}var i=window.Waypoint;e.prototype.createWaypoints=function(){for(var t={vertical:[{down:"enter",up:"exited",offset:"100%"},{down:"entered",up:"exit",offset:"bottom-in-view"},{down:"exit",up:"entered",offset:0},{down:"exited",up:"enter",offset:function(){return-this.adapter.outerHeight()}}],horizontal:[{right:"enter",left:"exited",offset:"100%"},{right:"entered",left:"exit",offset:"right-in-view"},{right:"exit",left:"entered",offset:0},{right:"exited",left:"enter",offset:function(){return-this.adapter.outerWidth()}}]},e=0,i=t[this.axis].length;i>e;e++){var n=t[this.axis][e];this.createWaypoint(n)}},e.prototype.createWaypoint=function(t){var e=this;this.waypoints.push(new i({context:this.options.context,element:this.options.element,enabled:this.options.enabled,handler:function(t){return function(i){e.options[t[i]].call(e,i)}}(t),offset:t.offset,horizontal:this.options.horizontal}))},e.prototype.destroy=function(){for(var t=0,e=this.waypoints.length;e>t;t++)this.waypoints[t].destroy();this.waypoints=[]},e.prototype.disable=function(){for(var t=0,e=this.waypoints.length;e>t;t++)this.waypoints[t].disable()},e.prototype.enable=function(){for(var t=0,e=this.waypoints.length;e>t;t++)this.waypoints[t].enable()},e.defaults={context:window,enabled:!0,enter:t,entered:t,exit:t,exited:t},i.Inview=e}();