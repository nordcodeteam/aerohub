<?php get_header(); ?>

<?php include('inc/page_top_slider_block.php'); ?>

<?php
$fields = get_fields(get_queried_object_id()); 

if (isset($fields['statistics_block_header']) && ($fields['statistics_block_header'] != '')) {
	$header = $fields['statistics_block_header'];
} else if (isset($fields['post_block_header']) && ($fields['post_block_header'] != '')) {
	$header = $fields['post_block_header'];
} else {
	$header = get_queried_object()->post_title;
}
?>
<div class="arch-top">
    <div class="scroll-to" data-block-id="index-main-content" data-slider-scroll="true">
        <div class="bg"></div>   
        <div class="text"><?php echo $header; ?></div>   
    </div>
    <div class="arch"></div>
</div>

<div class="content-wrap">
    <?php include('inc/page_stats_block.php'); ?>
</div>

<div class="content-wrap">
    <?php include('inc/page_post_blocks.php'); ?>
</div>
<?php get_footer(); ?>