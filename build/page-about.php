<?php /* Template Name: About page */ ?>
<?php get_header(); ?>
<?php include('inc/page_top_slider_block.php'); ?>

<?php
$post = get_queried_object();

$pattern = "/<h(.*?)<\/h/";
preg_match($pattern, wpautop($post->post_content), $matches);

$heading = '';
$content = '';
if (isset($matches[1]) && ($matches[1] != '')) {
    $heading = explode('>', $matches[1]);
	$heading = $heading[1];

	$get_content = explode('>', $post->post_content, 2);

	if (!empty($get_content) && ($get_content[1] != '')) {
		$content = $get_content[1];
	}
}
?>

<div class="arch-top">
    <div class="scroll-to" data-block-id="index-main-content" data-slider-scroll="true">
        <div class="bg"></div>   
        <div class="text"><?php echo $heading; ?></div>   
    </div>
    <div class="arch"></div>
</div>
<div class="content-wrap">
	<?php if ($heading != '') : ?>
	<div class="grid-row">
		    <h2 class="grid-content-header top-margin"><?php echo $heading; ?></h2>
	</div>
	<?php endif; ?>

	<?php if ($content != '') : ?>
	<div class="grid-row">
		    <p class="indent"><?php echo $content; ?></p>
	</div>
	<?php endif; ?>
</div>

<div class="about-slides-wrap">
    <div class="about-slide west">
        <div class="svg">
            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 width="1920px" height="903px" viewBox="0 0 1920 903" enable-background="new 0 0 1920 903" xml:space="preserve">
            <g id="remas">
                <rect fill="none" stroke="#FFFFFF" stroke-miterlimit="10" stroke-opacity="0" width="1920" height="903"/>
            </g>
            <circle class="point-1" fill="#6296FF" fill-opacity="0" cx="1437.566" cy="358.095" r="1"/>
            <circle class="point-2" fill="#6296FF" fill-opacity="0" cx="1175.58" cy="579.084" r="1"/>
            <circle class="point-3" fill="#6296FF" fill-opacity="0" cx="768.601" cy="191.604" r="1"/>
            <circle class="point-4" fill="#6296FF" fill-opacity="0" cx="647.106" cy="373.095" r="1"/>
            </svg>

        </div>  
        <div class="tooltip tooltip-4 top-left" data-point="4">
            <div class="number">4</div>
            <div class="text">RWY<br>Runway / pakilimo /<br>nusileidimo takas<br>(3250 x 45m)</div>
            <div class="arrow"></div>
        </div> 
        <div class="tooltip tooltip-3 top-left" data-point="3">
            <div class="number">3</div>
            <div class="text">RWY<br>Runway / pakilimo /<br>nusileidimo takas<br>(3250 x 45m)</div>
            <div class="arrow"></div>
        </div>
        <div class="tooltip tooltip-2 bottom-left" data-point="2">
            <div class="number">2</div>
            <div class="text">RWY<br>Runway / pakilimo /<br>nusileidimo takas<br>(3250 x 45m)</div>
            <div class="arrow"></div>
        </div>
        <div class="tooltip tooltip-1 top-right" data-point="1">
            <div class="number">1</div>
            <div class="text">RWY<br>Runway / pakilimo /<br>nusileidimo takas<br>(3250 x 45m)</div>
            <div class="arrow"></div>
        </div>
    </div>
    <div class="about-slide south">
        <div class="svg">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="1920px"
                 height="903px" viewBox="0 0 1920 903" enable-background="new 0 0 1920 903" xml:space="preserve">
            <g id="remas">
                <rect opacity="0" fill="none" stroke="#FFFFFF" stroke-miterlimit="10" width="1920" height="903"/>
            </g>
            <g id="taskai">
                <circle class="point-1" opacity="0" fill="#6296FF" cx="1401.069" cy="245.45" r="1"/>
                <circle class="point-2" opacity="0" fill="#6296FF" cx="996.09" cy="718.426" r="1"/>
            </g>
            </svg>

        </div>
        <div class="tooltip tooltip-2 bottom-right" data-point="2">
            <div class="number">2</div>
            <div class="text">RWY<br>Runway / pakilimo /<br>nusileidimo takas<br>(3250 x 45m)</div>
            <div class="arrow"></div>
        </div>
        <div class="tooltip tooltip-1 top-left" data-point="1">
            <div class="number">1</div>
            <div class="text">RWY<br>Runway / pakilimo /<br>nusileidimo takas<br>(3250 x 45m)</div>
            <div class="arrow"></div>
        </div>
    </div>
    <div class="about-slide east">
        <div class="svg">
            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 width="1920px" height="903px" viewBox="0 0 1920 903" enable-background="new 0 0 1920 903" xml:space="preserve">
            <g id="remas">
                <rect fill="none" stroke="#FFFFFF" stroke-miterlimit="10" stroke-opacity="0" width="1920" height="903"/>
            </g>
            <circle class="point-1" fill="#6296FF" fill-opacity="0" cx="1050.586" cy="599.583" r="1"/>
            <circle class="point-2" fill="#6296FF" fill-opacity="0" cx="781.1" cy="161.105" r="1"/>
            <circle class="point-3" fill="#6296FF" fill-opacity="0" cx="591.609" cy="619.082" r="1"/>
            <circle class="point-4" fill="#6296FF" fill-opacity="0" cx="1692.054" cy="476.59" r="1"/>
            </svg>
        </div>  
        <div class="tooltip tooltip-4 bottom-right" data-point="4">
            <div class="number">4</div>
            <div class="text">RWY<br>Runway / pakilimo /<br>nusileidimo takas<br>(3250 x 45m)</div>
            <div class="arrow"></div>
        </div>
        <div class="tooltip tooltip-3 top-left" data-point="3">
            <div class="number">3</div>
            <div class="text">RWY<br>Runway / pakilimo /<br>nusileidimo takas<br>(3250 x 45m)</div>
            <div class="arrow"></div>
        </div>
        <div class="tooltip tooltip-2 top-left" data-point="2">
            <div class="number">2</div>
            <div class="text">RWY<br>Runway / pakilimo /<br>nusileidimo takas<br>(3250 x 45m)</div>
            <div class="arrow"></div>
        </div>
        <div class="tooltip tooltip-1 top-left" data-point="1">
            <div class="number">1</div>
            <div class="text">RWY<br>Runway / pakilimo /<br>nusileidimo takas<br>(3250 x 45m)</div>
            <div class="arrow"></div>
        </div>
    </div>
    <div class="about-slide north active">
        <div class="svg">
            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 width="1920px" height="903px" viewBox="0 0 1920 903" enable-background="new 0 0 1920 903" xml:space="preserve">
            <g id="remas">
                <rect fill="none" stroke="#FFFFFF" stroke-miterlimit="10" stroke-opacity="0" width="1920" height="903"/>
            </g>
            <circle class="point-1" fill="#6296FF" fill-opacity="0" cx="1317.072" cy="316.098" r="1"/>
            <circle class="point-2" fill="#6296FF" fill-opacity="0" cx="486.61" cy="410.084" r="1"/>
            </svg>
        </div>  
        <div class="tooltip tooltip-2 bottom-left" data-point="2">
            <div class="number">2</div>
            <div class="text">RWY<br>Runway / pakilimo /<br>nusileidimo takas<br>(3250 x 45m)</div>
            <div class="arrow"></div>
        </div>
        <div class="tooltip tooltip-1 top-right" data-point="1">
            <div class="number">1</div>
            <div class="text">RWY<br>Runway / pakilimo /<br>nusileidimo takas<br>(3250 x 45m)</div>
            <div class="arrow"></div>
        </div>
    </div>

    <div class="compass north">
        <div class="item west"><span class="text">W</span></div>
        <div class="item south"><span class="text">S</span></div>
        <div class="item east"><span class="text">E</span></div>
        <div class="item north"><span class="text">N</span></div>
    </div>

    <div class="navigation">
        <div class="item north active" data-slide="north">N</div>
        <div class="item east" data-slide="east">E</div>
        <div class="item south" data-slide="south">S</div>
        <div class="item west" data-slide="west">W</div>
    </div>
</div>

<div class="location-buttons about">
    <div class="location-button north active" data-tab="north">North</div>
    <div class="location-button east" data-tab="east">East</div>
    <div class="location-button south" data-tab="south">South</div>
    <div class="location-button west" data-tab="west">West</div>
</div>
<div class="mobile-content-popup-wrap"> 
    <div class="mobile-content-popup north active">

        <div class="content">
            <div class="pop-content">            
                <div class="image"></div>     
            </div>
        </div>
    </div>
    <div class="mobile-content-popup east">

        <div class="content">
            <div class="pop-content">            
                <div class="image"></div>    
            </div>
        </div>
    </div>
    <div class="mobile-content-popup south">

        <div class="content">
            <div class="pop-content">            
                <div class="image"></div>     
            </div>
        </div>
    </div>
    <div class="mobile-content-popup west">

        <div class="content">
            <div class="pop-content">            
                <div class="image"></div>     
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>