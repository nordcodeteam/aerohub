<?php 
/* Template Name: Contacts */
get_header();


$pages = get_posts(array(
    'post_type' => 'page',
    'meta_key' => '_wp_page_template',
    'meta_value' => 'page-contacts.php'
));

$fields = array();

if (!empty($pages)) {
    $page = $pages[0];
    $fields = get_fields($page->ID);
}
?>

<script type="text/javascript">
function initMap() {
	var myLatLng = {lat: <?php echo $fields['contacts_map']['lat'] ?>, lng: <?php echo $fields['contacts_map']['lng'] ?>};

	var customMapType = new google.maps.StyledMapType([
		{ 
			"stylers": [ { "saturation": -100 }, { "gamma": 1.32 }, { "lightness": 26 } ] 
		},
		{ 
			"elementType": "geometry.stroke", "stylers": [ { "color": "#7995d8" } ] 
		},
		{ 
			"elementType": "labels.text.fill", "stylers": [ { "color": "#808080" }, { "lightness": -26 } ] 
		} 
	]);
	var customMapTypeId = 'custom_style';

	// Create a map object and specify the DOM element for display.
	var map = new google.maps.Map(document.getElementById('map'), {
		center: myLatLng,
		scrollwheel: false,
		zoom: 12
	});

	// Create a marker and set its position.
	var marker = new google.maps.Marker({
		map: map,
		position: myLatLng,
	});

	map.mapTypes.set(customMapTypeId, customMapType);
	map.setMapTypeId(customMapTypeId);
}
</script>

	<div class="contacts-wrap">
		<div class="contact-info-wrap">
			<h2 class="grid-content-header top-margin green-border">Contacts</h2>

			<div class="contact-info-blocks">
	            <?php if ((!empty($fields['info_rows'])) || ($fields['full_address'] != '')) : ?>
	            <div class="contact-info-block info-rows">
	    
	                <?php if (!empty($fields['info_rows'])) : ?>
	                    <?php foreach ($fields['info_rows'] as $row) : ?>
	                        <div class="row"><?php echo $row['content']; ?></div>
	                    <?php endforeach; ?>
	                <?php endif; ?>

	            </div>
	            <?php endif; ?>

	            <?php if (!empty($fields['link_rows'])) : ?>
	            <div class="contact-info-block link-rows">
	    
	                <?php foreach ($fields['link_rows'] as $row) : ?>
	                    <div class="row <?php echo ($row['underline_link']) ? 'underline' : ''; ?>">

	                        <?php 
	                            $link
	                        ?>
	                        <a href="
	                        <?php 
	                        echo ($row['link_type'] == 'phone') ? 'tel:' : ''; 
	                        echo ($row['link_type'] == 'email') ? 'mailto:' : ''; 
	                        echo $row['link']; ?>">

	                        <?php echo $row['link']; ?></a>
	                    </div>
	                <?php endforeach; ?>

	            </div>
	            <?php endif; ?>

	            <?php if ($fields['linkedin_link'] != '') : ?>
	            <div class="contact-info-block linkedin">
	                <div class="row"><a class="underline" href="$fields['linkedin_link']">Linkedin</a></div>
	            </div>
	            <?php endif; ?>
			</div>
		</div>
		<div class="map-holder">
			<div id="map"></div>
		</div>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgFG0ZUAvP62im_E6dLiTRyrb3CfIqVvY&callback=initMap"
	        async defer></script>
	</div>

    <?php wp_footer(); ?>
    </body>
</html>