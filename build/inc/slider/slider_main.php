<div class="content-wrap main-slide-wrap">
    <div class="grid-row">
		<div class="main-slide-content">
			
			<?php
			$slider_fields = get_queried_object(); 
			$slider_fields = get_fields($slider_fields->ID);
			?>

			<?php if ($slider_fields['main_slider_header_text'] != '') : ?>
				<div class="header"><?php echo $slider_fields['main_slider_header_text']; ?></div>
			<?php endif; ?>

			<?php if (!empty($slider_fields['main_slider_content_text'])) : ?>
				<div class="content">
					<?php foreach ($slider_fields['main_slider_content_text'] as $row): ?>
						<a href="<?php echo get_permalink($row['row']->ID); ?>" alt="<?php echo $row['row']->post_title; ?>"><?php echo $row['row']->post_title; ?></a><br>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
			<?php 
				$about = get_posts(array(
				    'post_type' => 'page',
				    'meta_key' => '_wp_page_template',
				    'meta_value' => 'page-about.php'
				));

				if (!empty($about)) {
				    $about = $about[0];
			?>
		        <div class="read-more"><a href="<?php echo get_permalink($about->ID); ?>" alt="More">More</a></div>
			<?php } ?>

		</div>
	</div>
</div>
