<?php $blocks = get_page_blocks(get_the_ID()); ?>
<?php if (!empty($blocks)) : ?>
<div class="grid-row post-blocks">


    <?php foreach ($blocks as $block) : ?>
    <div class="grid-col post-block">
        <div class="block-image">
            <a href="<?php echo $block['url']; ?>"><img src="<?php echo $block['thumb']; ?>" alt="<?php echo $block['title']; ?>" /></a>
        </div>
        
        <div class="block-title">
            <a href="<?php echo $block['url']; ?>"><?php echo $block['title']; ?></a>
        </div>  

        <div class="block-content">
            <?php echo $block['description']; ?>
        </div>
    </div>
    <?php endforeach; ?>


</div>
<?php endif; ?>