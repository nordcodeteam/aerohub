<?php
    // Burger menu setup
    $burger_items = wp_get_nav_menu_items( 'burger-menu' ); 
    $burger_item_childs = array();
    $burger_single_items[] = array();


    //Seting up burger menu items child array with parent id as key
    $i = 0;
    foreach ($burger_items as $item) {
        if (isset($item->menu_item_parent) &&  ($item->menu_item_parent != 0)) {
            $burger_item_childs[$item->menu_item_parent][] = $item;
            unset($burger_items[$i]);
        }

        $i++;
    }

    //Separating items with childs from single ones
    $burger_items_with_childs = array();
    foreach ($burger_items as $item) {
        if (isset($burger_item_childs[$item->ID]) &&  !empty($burger_item_childs[$item->ID])) {
            $burger_items_with_childs[] = $item;
        } else {
            $burger_single_items[] = $item;
        }
    }


    $contact = get_posts(array(
        'post_type' => 'page',
        'meta_key' => '_wp_page_template',
        'meta_value' => 'page-contacts.php'
    ));

    $fields = array();

    if (!empty($contact)) {
        $contact = $contact[0];
        $fields = get_fields($contact->ID);
    }
?>

<!-- Burger Menu -->
<?php if (!empty($burger_items)) : ?>

<div id="burger-wrap" class="burger-wrap">
    <div class="content-wrap">
        <div class="burger-menu grid-row">

        <!-- Burger menu item blocks with childs -->
        <?php foreach ($burger_items_with_childs as $burger_block) : ?>
            <div class="burger-menu-column grid-col">
                <div class="burger-menu-column-header">
                    <a href="<?php echo $burger_block->url; ?>" alt="<?php echo $burger_block->post_title; ?>"><?php echo $burger_block->post_title; ?></a>
                </div>
                

                <?php if (isset($burger_item_childs[$burger_block->ID]) && !empty($burger_item_childs[$burger_block->ID])) : ?>
                <div class="burger-menu-column-list">
                    <?php $childs = $burger_item_childs[$burger_block->ID]; ?>
                    <?php foreach ($childs as $child) : ?>
                    
                    <div class="burger-menu-column-list-item">
                        <a href="<?php echo $child->url; ?>" alt="<?php echo $child->post_title; ?>"><?php echo $child->title; ?></a>
                    </div>

                    <?php endforeach; ?>
                </div>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
        <!-- END: Burger menu item blocks with childs -->

        <!-- Burger menu item block single items -->
        <?php if (isset($burger_single_items) && !empty($burger_single_items)) : ?>
            
            <div class="burger-menu-column grid-col">
                
                <?php foreach ($burger_single_items as $item) : ?>
                    <?php if (!empty($item)) : ?>

                    <div class="burger-menu-column-header single">
                        <a href="<?php echo $item->url; ?>" alt="<?php echo $item->post_title; ?>"><?php echo $item->title; ?></a>
                    </div>

                    <?php endif; ?>
                <?php endforeach; ?>

            </div>
        
        <?php endif; ?>
        <!-- END: Burger menu item block single items -->
            
        </div>



        <div class="burger-contacts grid-row">
            <?php if ((!empty($fields['info_rows'])) || ($fields['full_address'] != '')) : ?>
            <div class="contact-info-block info-rows grid-col">
    
                <?php if (!empty($fields['info_rows'])) : ?>
                    <?php foreach ($fields['info_rows'] as $row) : ?>
                        <div class="row"><?php echo $row['content']; ?></div>
                    <?php endforeach; ?>
                <?php endif; ?>

            </div>
            <?php endif; ?>

            <?php if (!empty($fields['link_rows'])) : ?>
            <div class="contact-info-block link-rows grid-col">
    
                <?php foreach ($fields['link_rows'] as $row) : ?>
                    <div class="row <?php echo ($row['underline_link']) ? 'underline' : ''; ?>">

                        <?php 
                            $link
                        ?>
                        <a href="
                        <?php 
                        echo ($row['link_type'] == 'phone') ? 'tel:' : ''; 
                        echo ($row['link_type'] == 'email') ? 'mailto:' : ''; 
                        echo $row['link']; ?>">

                        <?php echo $row['link']; ?></a>
                    </div>
                <?php endforeach; ?>

            </div>
            <?php endif; ?>

            <?php if ($fields['linkedin_link'] != '') : ?>
            <div class="contact-info-block linkedin grid-col">
                <div class="row"><a class="underline" target="_blank" href="<?php echo $fields['linkedin_link']; ?>">Linkedin</a></div>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php endif; ?>
<!-- END: Burger Menu -->