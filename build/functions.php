<?php // ==== FUNCTIONS ==== //

// Load the configuration file for this installation; all options are set here
if ( is_readable( trailingslashit( get_stylesheet_directory() ) . 'functions-config.php' ) )
  require_once( trailingslashit( get_stylesheet_directory() ) . 'functions-config.php' );
// An example of how to manage loading front-end assets (scripts, styles, and fonts)
require_once( trailingslashit( get_stylesheet_directory() ) . 'inc/assets.php' );

// Only the bare minimum to get the theme up and running
function voidx_setup() {

  // Language loading
  load_theme_textdomain( 'voidx', trailingslashit( get_template_directory() ) . 'languages' );

  // HTML5 support; mainly here to get rid of some nasty default styling that WordPress used to inject
  add_theme_support( 'html5', array( 'search-form', 'gallery' ) );

  // Automatic feed links
  add_theme_support( 'automatic-feed-links' );

  // $content_width limits the size of the largest image size available via the media uploader
  // It should be set once and left alone apart from that; don't do anything fancy with it; it is part of WordPress core
  global $content_width;
  if ( !isset( $content_width ) || !is_int( $content_width ) )
    $content_width = (int) 960;

  // Register header and footer menus
  register_nav_menu( 'header', __( 'Header menu', 'voidx' ) );
  register_nav_menu( 'footer', __( 'Footer menu', 'voidx' ) );

}
add_action( 'after_setup_theme', 'voidx_setup', 11 );
add_filter('show_admin_bar', '__return_false');

function get_page_blocks($id) {
    $fields = get_fields($id);

    $blocks = array();
    if (isset($fields['blocks']) && !empty($fields['blocks'])) {
        $block_data = $fields['blocks'];

        foreach ($block_data as $block) {
            $post_content = get_extended( $block['block_object']->post_content );
            $post_fields = get_fields( $block['block_object']->ID );
            
            $post_thumbnail = $post_fields['thumbnail'];
            $post_thumbnail_url = '';

            if ($block['post_blocks_use_post_object']) {            
              if (!empty($post_thumbnail) && ($post_thumbnail != '')) {
                  $post_thumbnail_url = $post_thumbnail['url'];
              }
            } else {
              if (!empty($block['block_image']) && ($block['block_image'] != '')) {
                  $post_thumbnail_url = $block['block_image'];
              }
            }

            if (($block['use_custom_description'] == true) && ($block['description'] != '')) {
                $post_excerpt = $block['description'];
            } else {
                $post_excerpt = strip_tags($post_content['main']);
            }

            if (($block['use_custom_title'] == true) && ($block['custom_title'] != '')) {
                $post_title = $block['custom_title'];
            } else {
                $post_title = $block['block_object']->post_title;
            }

            $post_url = get_permalink($block['block_object']->ID);

            $blocks[] = array(
                'static' => !$block['post_blocks_use_post_object'],
                'url' => $post_url,
                'title' => $post_title,
                'description' => $post_excerpt,
                'thumb' => $post_thumbnail_url
            );
        }
    }
    return $blocks;
}

// Sidebar declaration
function voidx_widgets_init() {
  register_sidebar( array(
    'name'          => __( 'Main sidebar', 'voidx' ),
    'id'            => 'sidebar-main',
    'description'   => __( 'Appears to the right side of most posts and pages.', 'voidx' ),
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h2>',
    'after_title'   => '</h2>'
  ) );
}
add_action( 'widgets_init', 'voidx_widgets_init' );


function catch_that_image($post) {
  $first_img = '';
  ob_start();
  ob_end_clean();
  $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
  $first_img = $matches [1] [0];

  if(empty($first_img)){
    $first_img = "";
  }
  return $first_img;
}

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');


if ( ! defined( 'USE_LOCAL_ACF_CONFIGURATION' ) || ! USE_LOCAL_ACF_CONFIGURATION ) {
  require_once dirname( __FILE__ ) . '/functions-config.php';
}

