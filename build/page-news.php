<?php /* Template Name: News */ ?>

<?php get_header(); ?>

<?php

global $wp_query;
$wp_query->query_vars['page'];

$posts_per_page = 6; 

$offest = 0;
$current_page = $wp_query->query_vars['page'];
if ($current_page > 1) {
    $offest = ($wp_query->query_vars['page'] - 1) * $posts_per_page;
}
$posts = get_posts(
    array(
        'post_type'         => 'post',
        'posts_per_page'    => $posts_per_page,
        'offset'            => $offest,
        'orderby'           => 'date',
        'order'             => 'DESC',
    )
);

$count_posts = wp_count_posts();
$published_posts = $count_posts->publish;

?>

<div class="content-wrap">
    <div class="grid-row">
        <h2 class="grid-content-header top-margin green-border">Latest news</h2>
    </div>
    
    <?php if (!empty($posts)) : ?>
    <div class="grid-row">
        <div class="post-items">       
            <?php foreach ($posts as $post) : ?>
            <div class="post-item">
                <?php $post_content = get_extended( $post->post_content ); ?>
                <div class="post-item-image" style="background-image: url('<?php echo catch_that_image($post); ?>');"></div>
                <div class="post-item-content-wrap">       
                    <div class="post-item-title"><?php echo $post->post_title; ?></div>
                    <div class="post-item-date"><span><?php echo date('Y m d', strtotime($post->post_date)); ?></span></div>
                    <div class="post-item-content"><?php echo strip_tags($post_content['main']); ?></div>
                </div>
                <a href="<?php echo get_permalink($post->ID); ?>"></a>
            </div>
            <?php endforeach; ?>   
            <div class="post-item empty"></div> 
            <div class="post-item empty"></div> 
            <div class="post-item empty"></div> 
        </div>
    </div>
    <?php endif; ?>
    
    <?php if (($published_posts > 0) && (ceil($published_posts/$posts_per_page) > 1)) : ?>
    <div class="grid-row">
        <div class="pagination">
            <div class="pagination-link prev pagination-item">
            <?php if ($wp_query->query_vars['page'] > 1) : ?>
                <a href="<?php echo get_bloginfo('url').'/'.$wp_query->query_vars['pagename'].'/'.($wp_query->query_vars['page'] - 1); ?>">Newer posts</a>
            <?php endif; ?>
            </div>

            <div class="current-page pagination-item">
                <?php echo ($current_page != 0) ? $current_page : 1; ?> of <?php echo ceil($published_posts/$posts_per_page); ?>
            </div>
            
            <div class="pagination-link next pagination-item">
            <?php if ($current_page < ceil($published_posts/$posts_per_page)) : ?>
                <?php $next_page = ($current_page > 0) ? ($current_page + 1) : 2; ?>
                <a href="<?php echo get_bloginfo('url').'/'.$wp_query->query_vars['pagename'].'/'.$next_page; ?>">Older posts</a>
            <?php endif; ?>
            </div>
        </div>
    </div>
    <?php endif; ?>

</div>

<?php get_footer(); ?>