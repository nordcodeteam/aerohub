    <?php
    $pages = get_posts(array(
        'post_type' => 'page',
        'meta_key' => '_wp_page_template',
        'meta_value' => 'page-contacts.php'
    ));
    $fields = array();

    if (!empty($pages)) {
        $page = $pages[0];

        $fields = get_fields($page->ID);
    }
    ?>

    <footer>
        <?php if (!empty($fields)) : ?>
            <div class="contact-wrap" 
                <?php echo ($fields['footer_background'] != '') ? 'style="background-image: url(\''.$fields['footer_background'].'\');"' : '';?>>
                <div class="content-wrap">
                    <?php if (!empty($fields)) : ?>
                    <div class="grid-row">
                        <h2 class="grid-content-header top-large-margin green-border white">Contacts</h2>
                    </div>
                    
                    <div class="grid-row post-blocks">

                        <?php if ((!empty($fields['info_rows'])) || ($fields['full_address'] != '')) : ?>
                        <div class="grid-col post-block info-rows">
                
                            <?php if (!empty($fields['info_rows'])) : ?>
                                <?php foreach ($fields['info_rows'] as $row) : ?>
                                    <div class="row"><?php echo $row['content']; ?></div>
                                <?php endforeach; ?>
                            <?php endif; ?>

                            <?php if ($fields['full_address'] != '') : ?>
                                <div class="row underline"><a href="http://maps.google.com/?q=<?php echo $fields['full_address']; ?>">View in map</a></div>
                            <?php endif; ?>

                        </div>
                        <?php endif; ?>

                        <?php if (!empty($fields['link_rows'])) : ?>
                        <div class="grid-col post-block link-rows bg-padding">
                
                            <?php foreach ($fields['link_rows'] as $row) : ?>
                                <div class="row <?php echo ($row['underline_link']) ? 'underline' : ''; ?>">

                                    <?php 
                                        $link
                                    ?>
                                    <a href="
                                    <?php 
                                    echo ($row['link_type'] == 'phone') ? 'tel:' : ''; 
                                    echo ($row['link_type'] == 'email') ? 'mailto:' : ''; 
                                    echo $row['link']; ?>">

                                    <?php echo $row['link']; ?></a>
                                </div>
                            <?php endforeach; ?>

                        </div>
                        <?php endif; ?>

                        <?php if ($fields['linkedin_link'] != '') : ?>
                        <div class="grid-col post-block linkedin bg-padding">
                                <div class="row"><a href="<?php echo $fields['linkedin_link']; ?>">Linkedin</a></div>
                        </div>
                        <?php endif; ?>


                    </div>
                    <?php endif; ?>

                </div>
            </div>


            <?php if ($fields['copyright_text'] != '') : ?>
            <div class="copyrights">
                <div class="content-wrap">
                    <div class="grid-row">
                        <div class="text"><?php echo $fields['copyright_text']; ?></div>
                    </div>
                </div>
            </div>
            <?php endif; ?>
        <?php endif; ?>
    </footer>

    <?php wp_footer(); ?>
    </body>
</html>