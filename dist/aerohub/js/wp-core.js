// ==== CORE ==== //

;(function($){
	var burgerMenu = {
		init: function() {
			var button = $('header .burger-menu-icon');
			var menu = $('#burger-wrap');
			button.bind('click tap', this.toggleMenu);

		},

		close: function() {
			var button = $('header .burger-menu-icon');
			var menu = $('#burger-wrap');

			button.removeClass('active');
			menu.removeClass('active');
            $('body').removeClass('disable-scroll');
            $('body').removeClass('fixed');
        },  
        open: function() {
            var button = $('header .burger-menu-icon');
            var menu = $('#burger-wrap');

            button.addClass('active');
            menu.addClass('active');
            $('body').addClass('disable-scroll');
            $('body').addClass('fixed');
        },

        toggleMenu: function() {
            var button = $('header .burger-menu-icon');
            var menu = $('#burger-wrap');

            button.toggleClass('active');
            button.addClass('clicked');
            menu.toggleClass('active');
            $('body').toggleClass('disable-scroll');

            if ($('body').hasClass('fixed')) {
                $('body').removeClass('fixed');
            } else {
                $('body').addClass('fixed');
            }
		}
	};

    var scrollTo = {
        init: function() {
            $('.scroll-to').bind('click tap', function(){
                if ($(this).data('slider-scroll') == true) {
                    $('html, body').animate({
                        scrollTop: $('.page-top-slider-wrap').offset().top + $('.page-top-slider-wrap').outerHeight() + $('.arch-top').outerHeight()
                    }, 300);
                } else {              
                    $('html, body').animate({
                        scrollTop: $('#' + $(this).data('block-id')).offset().top 
                    }, 300);
                }
            });
        }
    };

    var inview = {
        init: function() {
            var delayTime = 300;
            if ($('.post-blocks .post-block').length != 0) {     
                $('.post-blocks').each(function(){
                    $.each($(this).find('.post-block'), function(index, value){
                        new Waypoint.Inview({
                            element: $(value)[0],
                            enter: function(direction) {
                                setTimeout(function(){
                                    $(value)
                                        .removeClass('hide-down')
                                        .removeClass('hide-up')
                                        .addClass('show');

                                }, (index*delayTime));
                            },
                            entered: function(direction) {
                                $(value).removeClass('hide-up').removeClass('hide-down');

                                setTimeout(function(){
                                    $(value).addClass('show');
                                }, (index*delayTime));
                            },
                            exited: function(direction) {
                                $(value)
                                    .removeClass('show')
                                    .addClass('hide-' + direction);
                            }
                        })
                    });
                });     
            }

            if ($('.news-block').length != 0) {
                new Waypoint.Inview({
                    element: $('.news-block .news-content')[0],
                    entered: function(direction) {
                        $('.news-block')
                            .removeClass('hide')
                            .addClass('show');
                    }
                });
                new Waypoint.Inview({
                    element: $('.news-block')[0],
                    exited: function(direction) {
                        $('.news-block')
                            .removeClass('show')
                            .addClass('hide');
                    }
                });
            }
        } 
    };

    var mapAnimation = {
        position: function(){
            $('header .circle-holder').css({
                left: $('header .map-svg #sostine').offset().left,
                top: $('header .map-svg #sostine').offset().top 
            });
            $('header .map-popup-holder').css({
                left: $('header .map-svg #sostine').offset().left - $('header .map-popup').width(),
                top: $('header .map-svg #sostine').offset().top - $('header .map-popup').height() - $('header .map-popup .arrow').outerHeight() - 10
            });
        },
        init: function(){
            $('header .slider-overlay').fadeOut(500, function(){
                $('header .circle-holder').addClass('animate');
                $('header .main-slide-wrap').addClass('animate');
                $('header .circle-holder .circle-1').one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function() {
                    $('header .text-circle-wrapper').addClass('animate');
                    $('header .map-popup-holder').addClass('animate');
                });
            });
        }
    };

    var scrollTop = 0;
    var kaunasMap = {
        position: function() {
            $('.kaunas-map .map-bg .map-tab.kaunas').css({
                left: $('.kaunas-map .map-bg #point-kaunas').offset().left - 80,
                top: $('.kaunas-map .map-bg #point-kaunas').offset().top - 34 - jQuery('.kaunas-map').offset().top
            });
            $('.kaunas-map .map-bg .map-tab.e67').css({
                left: $('.kaunas-map .map-bg #point-e67').offset().left  - 10,
                top: $('.kaunas-map .map-bg #point-e67').offset().top - jQuery('.kaunas-map').offset().top
            });
            $('.kaunas-map .map-bg .map-tab.e85').css({
                left: $('.kaunas-map .map-bg #point-e85').offset().left - 20,
                top: $('.kaunas-map .map-bg #point-e85').offset().top - jQuery('.kaunas-map').offset().top
            });
            $('.kaunas-map .map-bg .map-tab.aerohub').css({
                left: $('.kaunas-map .map-bg #point-aerohub').offset().left,
                top: $('.kaunas-map .map-bg #point-aerohub').offset().top + 10 - jQuery('.kaunas-map').offset().top
            });
        },
        init: function() {
            $('.kaunas-map .location-tab').bind('click tap', function(){
                if ($(this).data('tab') == "kaunas") {
                    $('.kaunas-map svg #kaunas_stroke path').attr('stroke', '#00c17a');
                    $('.kaunas-map svg #kaunas_fill path').attr('fill', '#e0f1eb');
                } else {
                    $('.kaunas-map svg #kaunas_stroke path').attr('stroke', '#6787D2');
                    $('.kaunas-map svg #kaunas_fill path').attr('fill', '#E8E8E8');
                }

                if ($(this).data('tab') == "aerohub") {
                    $('.kaunas-map svg #aerohub path').attr('fill', '#00c17a');
                } else {
                    $('.kaunas-map svg #aerohub path').attr('fill', '#94A6CF');
                }
                if ($(this).data('tab') == "e67") {
                    $('.kaunas-map svg #E67 path').attr('stroke', '#00c17a');
                } else {
                    $('.kaunas-map svg #E67 path').attr('stroke', '#859DD9');
                }
                if ($(this).data('tab') == "e85") {
                    $('.kaunas-map svg #E85 path').attr('stroke', '#00c17a');
                } else {
                    $('.kaunas-map svg #E85 path').attr('stroke', '#859DD9');
                }

                $('.kaunas-map .location-tab').removeClass('active');
                $(this).addClass('active');
                $('.kaunas-map .map-bg .map-tab').removeClass('active');
                $('.kaunas-map .map-bg .map-tab.' + $(this).data('tab')).addClass('active');

            });


            $('.location-buttons .location-button').bind('click tap', function(){
                scrollTop = $(window).scrollTop();
                $('body').addClass('fixed');

                $('.mobile-content-popup.' + $(this).data('tab')).addClass('active');


            });
            $('.mobile-content-popup .read-more a').bind('click tap', function(e){
                e.preventDefault();
                console.log(scrollTop);
                $('body').removeClass('fixed');
                $('html, body').animate({
                    scrollTop: scrollTop
                }, 1);
                $('.mobile-content-popup').removeClass('active');
            });
        }
    };
    function resizeFullscreen() {
        // if ($('.full-screen').length != 0) {
        //     $('.full-screen').each(function(){
        //         $(this).css('height', $(window).height() - $(this).data('offset'));
        //     });
        // }
    }
	$(function(){
        burgerMenu.init();
        scrollTo.init();
        inview.init();

        if ($('header .map-svg').length != 0) {
            mapAnimation.position();
        }
        if ($('.kaunas-map').length != 0) {
            kaunasMap.position();
            kaunasMap.init();
        }
        resizeFullscreen();
    });

    $(window).load(function(){
        if ($('header .map-svg').length != 0) {
            mapAnimation.init();
        }
    });
    $(window).resize(function(){
        resizeFullscreen();
        if ($('header .map-svg').length != 0) {
            mapAnimation.position();
        }
        if ($('.kaunas-map').length != 0) {
            kaunasMap.position();
        }
    });
}(jQuery));


/*!
Waypoints - 4.0.0
Copyright © 2011-2015 Caleb Troughton
Licensed under the MIT license.
https://github.com/imakewebthings/waypoints/blog/master/licenses.txt
*/
!function(){"use strict";function t(o){if(!o)throw new Error("No options passed to Waypoint constructor");if(!o.element)throw new Error("No element option passed to Waypoint constructor");if(!o.handler)throw new Error("No handler option passed to Waypoint constructor");this.key="waypoint-"+e,this.options=t.Adapter.extend({},t.defaults,o),this.element=this.options.element,this.adapter=new t.Adapter(this.element),this.callback=o.handler,this.axis=this.options.horizontal?"horizontal":"vertical",this.enabled=this.options.enabled,this.triggerPoint=null,this.group=t.Group.findOrCreate({name:this.options.group,axis:this.axis}),this.context=t.Context.findOrCreateByElement(this.options.context),t.offsetAliases[this.options.offset]&&(this.options.offset=t.offsetAliases[this.options.offset]),this.group.add(this),this.context.add(this),i[this.key]=this,e+=1}var e=0,i={};t.prototype.queueTrigger=function(t){this.group.queueTrigger(this,t)},t.prototype.trigger=function(t){this.enabled&&this.callback&&this.callback.apply(this,t)},t.prototype.destroy=function(){this.context.remove(this),this.group.remove(this),delete i[this.key]},t.prototype.disable=function(){return this.enabled=!1,this},t.prototype.enable=function(){return this.context.refresh(),this.enabled=!0,this},t.prototype.next=function(){return this.group.next(this)},t.prototype.previous=function(){return this.group.previous(this)},t.invokeAll=function(t){var e=[];for(var o in i)e.push(i[o]);for(var n=0,r=e.length;r>n;n++)e[n][t]()},t.destroyAll=function(){t.invokeAll("destroy")},t.disableAll=function(){t.invokeAll("disable")},t.enableAll=function(){t.invokeAll("enable")},t.refreshAll=function(){t.Context.refreshAll()},t.viewportHeight=function(){return window.innerHeight||document.documentElement.clientHeight},t.viewportWidth=function(){return document.documentElement.clientWidth},t.adapters=[],t.defaults={context:window,continuous:!0,enabled:!0,group:"default",horizontal:!1,offset:0},t.offsetAliases={"bottom-in-view":function(){return this.context.innerHeight()-this.adapter.outerHeight()},"right-in-view":function(){return this.context.innerWidth()-this.adapter.outerWidth()}},window.Waypoint=t}(),function(){"use strict";function t(t){window.setTimeout(t,1e3/60)}function e(t){this.element=t,this.Adapter=n.Adapter,this.adapter=new this.Adapter(t),this.key="waypoint-context-"+i,this.didScroll=!1,this.didResize=!1,this.oldScroll={x:this.adapter.scrollLeft(),y:this.adapter.scrollTop()},this.waypoints={vertical:{},horizontal:{}},t.waypointContextKey=this.key,o[t.waypointContextKey]=this,i+=1,this.createThrottledScrollHandler(),this.createThrottledResizeHandler()}var i=0,o={},n=window.Waypoint,r=window.onload;e.prototype.add=function(t){var e=t.options.horizontal?"horizontal":"vertical";this.waypoints[e][t.key]=t,this.refresh()},e.prototype.checkEmpty=function(){var t=this.Adapter.isEmptyObject(this.waypoints.horizontal),e=this.Adapter.isEmptyObject(this.waypoints.vertical);t&&e&&(this.adapter.off(".waypoints"),delete o[this.key])},e.prototype.createThrottledResizeHandler=function(){function t(){e.handleResize(),e.didResize=!1}var e=this;this.adapter.on("resize.waypoints",function(){e.didResize||(e.didResize=!0,n.requestAnimationFrame(t))})},e.prototype.createThrottledScrollHandler=function(){function t(){e.handleScroll(),e.didScroll=!1}var e=this;this.adapter.on("scroll.waypoints",function(){(!e.didScroll||n.isTouch)&&(e.didScroll=!0,n.requestAnimationFrame(t))})},e.prototype.handleResize=function(){n.Context.refreshAll()},e.prototype.handleScroll=function(){var t={},e={horizontal:{newScroll:this.adapter.scrollLeft(),oldScroll:this.oldScroll.x,forward:"right",backward:"left"},vertical:{newScroll:this.adapter.scrollTop(),oldScroll:this.oldScroll.y,forward:"down",backward:"up"}};for(var i in e){var o=e[i],n=o.newScroll>o.oldScroll,r=n?o.forward:o.backward;for(var s in this.waypoints[i]){var a=this.waypoints[i][s],l=o.oldScroll<a.triggerPoint,h=o.newScroll>=a.triggerPoint,p=l&&h,u=!l&&!h;(p||u)&&(a.queueTrigger(r),t[a.group.id]=a.group)}}for(var c in t)t[c].flushTriggers();this.oldScroll={x:e.horizontal.newScroll,y:e.vertical.newScroll}},e.prototype.innerHeight=function(){return this.element==this.element.window?n.viewportHeight():this.adapter.innerHeight()},e.prototype.remove=function(t){delete this.waypoints[t.axis][t.key],this.checkEmpty()},e.prototype.innerWidth=function(){return this.element==this.element.window?n.viewportWidth():this.adapter.innerWidth()},e.prototype.destroy=function(){var t=[];for(var e in this.waypoints)for(var i in this.waypoints[e])t.push(this.waypoints[e][i]);for(var o=0,n=t.length;n>o;o++)t[o].destroy()},e.prototype.refresh=function(){var t,e=this.element==this.element.window,i=e?void 0:this.adapter.offset(),o={};this.handleScroll(),t={horizontal:{contextOffset:e?0:i.left,contextScroll:e?0:this.oldScroll.x,contextDimension:this.innerWidth(),oldScroll:this.oldScroll.x,forward:"right",backward:"left",offsetProp:"left"},vertical:{contextOffset:e?0:i.top,contextScroll:e?0:this.oldScroll.y,contextDimension:this.innerHeight(),oldScroll:this.oldScroll.y,forward:"down",backward:"up",offsetProp:"top"}};for(var r in t){var s=t[r];for(var a in this.waypoints[r]){var l,h,p,u,c,d=this.waypoints[r][a],f=d.options.offset,w=d.triggerPoint,y=0,g=null==w;d.element!==d.element.window&&(y=d.adapter.offset()[s.offsetProp]),"function"==typeof f?f=f.apply(d):"string"==typeof f&&(f=parseFloat(f),d.options.offset.indexOf("%")>-1&&(f=Math.ceil(s.contextDimension*f/100))),l=s.contextScroll-s.contextOffset,d.triggerPoint=y+l-f,h=w<s.oldScroll,p=d.triggerPoint>=s.oldScroll,u=h&&p,c=!h&&!p,!g&&u?(d.queueTrigger(s.backward),o[d.group.id]=d.group):!g&&c?(d.queueTrigger(s.forward),o[d.group.id]=d.group):g&&s.oldScroll>=d.triggerPoint&&(d.queueTrigger(s.forward),o[d.group.id]=d.group)}}return n.requestAnimationFrame(function(){for(var t in o)o[t].flushTriggers()}),this},e.findOrCreateByElement=function(t){return e.findByElement(t)||new e(t)},e.refreshAll=function(){for(var t in o)o[t].refresh()},e.findByElement=function(t){return o[t.waypointContextKey]},window.onload=function(){r&&r(),e.refreshAll()},n.requestAnimationFrame=function(e){var i=window.requestAnimationFrame||window.mozRequestAnimationFrame||window.webkitRequestAnimationFrame||t;i.call(window,e)},n.Context=e}(),function(){"use strict";function t(t,e){return t.triggerPoint-e.triggerPoint}function e(t,e){return e.triggerPoint-t.triggerPoint}function i(t){this.name=t.name,this.axis=t.axis,this.id=this.name+"-"+this.axis,this.waypoints=[],this.clearTriggerQueues(),o[this.axis][this.name]=this}var o={vertical:{},horizontal:{}},n=window.Waypoint;i.prototype.add=function(t){this.waypoints.push(t)},i.prototype.clearTriggerQueues=function(){this.triggerQueues={up:[],down:[],left:[],right:[]}},i.prototype.flushTriggers=function(){for(var i in this.triggerQueues){var o=this.triggerQueues[i],n="up"===i||"left"===i;o.sort(n?e:t);for(var r=0,s=o.length;s>r;r+=1){var a=o[r];(a.options.continuous||r===o.length-1)&&a.trigger([i])}}this.clearTriggerQueues()},i.prototype.next=function(e){this.waypoints.sort(t);var i=n.Adapter.inArray(e,this.waypoints),o=i===this.waypoints.length-1;return o?null:this.waypoints[i+1]},i.prototype.previous=function(e){this.waypoints.sort(t);var i=n.Adapter.inArray(e,this.waypoints);return i?this.waypoints[i-1]:null},i.prototype.queueTrigger=function(t,e){this.triggerQueues[e].push(t)},i.prototype.remove=function(t){var e=n.Adapter.inArray(t,this.waypoints);e>-1&&this.waypoints.splice(e,1)},i.prototype.first=function(){return this.waypoints[0]},i.prototype.last=function(){return this.waypoints[this.waypoints.length-1]},i.findOrCreate=function(t){return o[t.axis][t.name]||new i(t)},n.Group=i}(),function(){"use strict";function t(t){this.$element=e(t)}var e=window.jQuery,i=window.Waypoint;e.each(["innerHeight","innerWidth","off","offset","on","outerHeight","outerWidth","scrollLeft","scrollTop"],function(e,i){t.prototype[i]=function(){var t=Array.prototype.slice.call(arguments);return this.$element[i].apply(this.$element,t)}}),e.each(["extend","inArray","isEmptyObject"],function(i,o){t[o]=e[o]}),i.adapters.push({name:"jquery",Adapter:t}),i.Adapter=t}(),function(){"use strict";function t(t){return function(){var i=[],o=arguments[0];return t.isFunction(arguments[0])&&(o=t.extend({},arguments[1]),o.handler=arguments[0]),this.each(function(){var n=t.extend({},o,{element:this});"string"==typeof n.context&&(n.context=t(this).closest(n.context)[0]),i.push(new e(n))}),i}}var e=window.Waypoint;window.jQuery&&(window.jQuery.fn.waypoint=t(window.jQuery)),window.Zepto&&(window.Zepto.fn.waypoint=t(window.Zepto))}();
/*!
Waypoints Inview Shortcut - 4.0.0
Copyright © 2011-2015 Caleb Troughton
Licensed under the MIT license.
https://github.com/imakewebthings/waypoints/blog/master/licenses.txt
*/
!function(){"use strict";function t(){}function e(t){this.options=i.Adapter.extend({},e.defaults,t),this.axis=this.options.horizontal?"horizontal":"vertical",this.waypoints=[],this.element=this.options.element,this.createWaypoints()}var i=window.Waypoint;e.prototype.createWaypoints=function(){for(var t={vertical:[{down:"enter",up:"exited",offset:"100%"},{down:"entered",up:"exit",offset:"bottom-in-view"},{down:"exit",up:"entered",offset:0},{down:"exited",up:"enter",offset:function(){return-this.adapter.outerHeight()}}],horizontal:[{right:"enter",left:"exited",offset:"100%"},{right:"entered",left:"exit",offset:"right-in-view"},{right:"exit",left:"entered",offset:0},{right:"exited",left:"enter",offset:function(){return-this.adapter.outerWidth()}}]},e=0,i=t[this.axis].length;i>e;e++){var n=t[this.axis][e];this.createWaypoint(n)}},e.prototype.createWaypoint=function(t){var e=this;this.waypoints.push(new i({context:this.options.context,element:this.options.element,enabled:this.options.enabled,handler:function(t){return function(i){e.options[t[i]].call(e,i)}}(t),offset:t.offset,horizontal:this.options.horizontal}))},e.prototype.destroy=function(){for(var t=0,e=this.waypoints.length;e>t;t++)this.waypoints[t].destroy();this.waypoints=[]},e.prototype.disable=function(){for(var t=0,e=this.waypoints.length;e>t;t++)this.waypoints[t].disable()},e.prototype.enable=function(){for(var t=0,e=this.waypoints.length;e>t;t++)this.waypoints[t].enable()},e.defaults={context:window,enabled:!0,enter:t,entered:t,exit:t,exited:t},i.Inview=e}();