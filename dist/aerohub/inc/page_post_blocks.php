<?php $blocks = get_page_blocks(get_queried_object_id()); ?>
<?php if (!empty($blocks)) : ?>

<?php 
$fields = get_fields(get_queried_object_id()); 
if (isset($fields['post_block_header'])) : ?>

<div class="grid-row">
    <h2 class="grid-content-header top-margin <?php echo ($fields['large_post_block_header']) ? 'green-border' : ''; ?>"><?php echo $fields['post_block_header']; ?></h2>
</div>

<?php endif; ?>

<div class="grid-row post-blocks <?php echo ($fields['post_blocks_with_borders']) ? 'with-borders' : ''; ?>">

    <?php $i = 1; $count = count($blocks); ?>
    <?php foreach ($blocks as $block) : ?>
    <div class="grid-col post-block hide-up <?php echo ($i == $count) ? 'last' : ''; ?>">
        <div class="block-image">
            <?php if ($block['static']) : ?>
                <img src="<?php echo $block['thumb']; ?>" alt="<?php echo $block['title']; ?>" />
            <?php else : ?>
                <a href="<?php echo $block['url']; ?>"><img src="<?php echo $block['thumb']; ?>" alt="<?php echo $block['title']; ?>" /></a>
            <?php endif; ?>
        </div>
        
        <div class="block-title">
            <?php if ($block['static']) : ?>
                <a href="<?php echo $block['url']; ?>"><?php echo $block['title']; ?></a>
            <?php else : ?>
                <a href="<?php echo $block['url']; ?>"><?php echo $block['title']; ?></a>
            <?php endif; ?>
        </div>  

        <div class="block-content">
            <?php echo $block['description']; ?>
        </div>
    </div>
    <?php $i++; ?>
    <?php endforeach; ?>

    <div class="grid-col post-block empty"></div>
    <div class="grid-col post-block empty"></div>
    <div class="grid-col post-block empty"></div>

</div>
<?php endif; ?>