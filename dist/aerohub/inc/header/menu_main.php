<?php 
    $items = wp_get_nav_menu_items( 'main-menu' ); 
    $page_id = get_queried_object_id();
?>
<!-- Main Menu -->
<?php if (!empty($items)) : ?>


<div class="menu-holder">

    <?php foreach ($items as $item) : ?>

		<?php 
		$is_active = false;

		if ($page_id == $item->object_id) {
			$is_active = true;
		}

		$page_fields = get_fields($page_id);
		if (isset($page_fields['parent_page'])) {
			$parent_ID = $page_fields['parent_page']->ID;
			if ($parent_ID == $item->object_id) {
				$is_active = true;
			}
		}
		?>

    <div class="menu-item <?php echo ($is_active) ? 'active': ''; ?>"><a href="<?php echo $item->url; ?>" alt="<?php echo $item->title; ?>"><?php echo $item->title; ?></a></div>

    <?php endforeach; ?>

</div>

<div class="burger-menu-icon"><span></span></div>
<?php endif; ?>
<!-- END: Main Menu -->