<?php 
$args = array(
	'numberposts'	=> -1,
	'post_type'		=> array('invest', 'about'),
);
$posts = get_posts($args);

$child_posts = array();
$obj = get_queried_object();
$parent_page = null;


if ($obj->post_type == "page") {
    foreach ($posts as $post) {
        $post_fields = get_fields($post->ID);
        $post_parent = $post_fields['parent_page'];

		if ($post_parent->ID == $obj->ID) {
			$child_posts[] = $post;
		}
	}
} else {
	$current_post_fields = get_fields($obj->ID);
	$current_post_parent = $current_post_fields['parent_page'];

	if ($current_post_parent != null) {	
		foreach ($posts as $post) {
			$post_fields = get_fields($post->ID);
			$post_parent = $post_fields['parent_page'];

			if ($post_parent->ID == $current_post_parent->ID) {
				$child_posts[] = $post;
			}
		}
		$parent_page = $current_post_parent;
	}
}

?>


<?php if (!empty($child_posts)) : ?>

<div class="sub-menu-wrap">
	<?php /* */ ?>
	<?php if ($obj->post_type == "page") : ?>
	<div class="sub-menu-item active"><a href="<?php echo get_permalink($obj->ID); ?>"><?php echo $obj->post_title; ?></a></div>
	<?php else : ?>
	<div class="sub-menu-item"><a href="<?php echo get_permalink($parent_page->ID); ?>"><?php echo $parent_page->post_title; ?></a></div>
	<?php endif; ?>
	<? /**/ ?>

	<?php foreach($child_posts as $post) : ?>
	<div class="sub-menu-item <?php echo ($post->ID == $obj->ID) ? 'active' : ''; ?>"><a href="<?php echo get_permalink($post->ID); ?>"><?php echo $post->post_title; ?></a></div>
	<?php endforeach; ?>

</div>
	
<?php endif; ?>