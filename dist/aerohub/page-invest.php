<?php /* Template Name: Invest page */ ?>
<?php get_header(); ?>
<?php include('inc/page_top_slider_block.php'); ?>

<div class="content-wrap" id="index-main-content">
    <?php include('inc/page_post_blocks.php'); ?>
</div>

<?php get_footer(); ?>