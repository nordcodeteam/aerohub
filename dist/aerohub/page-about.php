<?php /* Template Name: About page */ ?>
<?php get_header(); ?>
<?php include('inc/page_top_slider_block.php'); ?>

<?php
$post = get_queried_object();

$pattern = "/<h(.*?)<\/h/";
preg_match($pattern, wpautop($post->post_content), $matches);

$heading = '';
$content = '';
if (isset($matches[1]) && ($matches[1] != '')) {
	$heading = explode('>', $matches[1])[1];

	$get_content = explode($heading, strip_tags($post->post_content));

	if (!empty($get_content) && ($get_content[1] != '')) {
		$content = $get_content[1];
	}
}
?>

<div class="content-wrap">
	<?php if ($heading != '') : ?>
	<div class="grid-row">
		    <h2 class="grid-content-header top-margin"><?php echo $heading; ?></h2>
	</div>
	<?php endif; ?>

	<?php if ($content != '') : ?>
	<div class="grid-row">
		    <p class="indent"><?php echo $content; ?></p>
	</div>
	<?php endif; ?>


</div>

<?php get_footer(); ?>